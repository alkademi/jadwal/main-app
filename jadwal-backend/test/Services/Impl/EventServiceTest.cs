using System;
using NUnit.Framework;
using Moq;
using jadwal_backend.Repositories;
using jadwal_backend.Helpers;
using jadwal_backend.Models;
using System.Linq;
using jadwal_backend.Models.DTO.Requests;
using jadwal_backend.Services;
using jadwal_backend.Services.Impl;
using jadwal_backend.Models.DTO.Responses;
using Newtonsoft.Json;
using jadwal_backend.Models.DTO;
using System.Collections.Generic;

namespace jadwal_backend_test;

public class EventServiceTest
{
    private Mock<IEventRepository> eventRepositoryMock;
    private Mock<IUserRepository> userRepositoryMock;
    private IEventService eventService;

    private static int EventId1 = 1;
    private static int EventId2 = 2;
    private static int OwnerId = 3;
    private static string Email = "Test Email";
    private static string Title = "Test Title";
    private static string Location = "Test Location";
    private static string Guests = "Test Guests";
    private static string Color = "Test Color";
    private static string Description = "Test Description";
    private static string Password = "Test Password";
    private static string FullName = "Test Fullname";
    private static string Phone = "Test Phone";
    private static DateTime StartDateTime = DateTime.Now.AddDays(-1);
    private static DateTime EndDateTime = DateTime.Now.AddDays(1);
    private static TimeSpan Reminder = DateTime.Now.AddDays(-1) - DateTime.Now.AddDays(-1).AddMinutes(-10);

    [SetUp]
    public void Setup()
    {
        eventRepositoryMock = new Mock<IEventRepository>();
        userRepositoryMock = new Mock<IUserRepository>();
        eventService = new EventService(eventRepositoryMock.Object, userRepositoryMock.Object);
    }

    [Test]
    public void AddEvent_ShouldSuccess()
    {
        User testUser = GetTestUser();
        eventRepositoryMock.Setup(e => e.Add(It.IsAny<Event>())).Verifiable();
        userRepositoryMock.Setup(u => u.FindByEmail(Email)).Returns(testUser);

        Response<string> actual = eventService.AddEvent(GetAddEventRequestDTO(), Email);

        Response<string> expected = GetAddEventResponse();
        eventRepositoryMock.Verify(e => e.Add(It.IsAny<Event>()), Times.Once());
        userRepositoryMock.Verify(s => s.FindByEmail(Email), Times.Once());
        Assert.AreEqual(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual));
    }

    [Test]
    public void GetEventById_EventNotFound_ShouldSuccess()
    {
        User testUser = GetTestUser();
        eventRepositoryMock.Setup(e => e.GetEventById(OwnerId, Email, EventId1)).Returns((Event)null);
        userRepositoryMock.Setup(u => u.FindByEmail(Email)).Returns(testUser);

        Response<FullEventDTO> actual = eventService.GetEventById(EventId1, Email);

        Response<FullEventDTO> expected = GetEventByIdResponse_EventNotFound();
        eventRepositoryMock.Verify(e => e.GetEventById(OwnerId, Email, EventId1), Times.Once());
        userRepositoryMock.Verify(s => s.FindByEmail(Email), Times.Once());
        Assert.AreEqual(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual));
    }

    [Test]
    public void GetEventById_EventFound_ShouldSuccess()
    {
        User testUser = GetTestUser();
        Event testEvent = GetTestEvent1();
        eventRepositoryMock.Setup(e => e.GetEventById(OwnerId, Email, EventId1)).Returns(testEvent);
        userRepositoryMock.Setup(u => u.FindByEmail(Email)).Returns(testUser);

        Response<FullEventDTO> actual = eventService.GetEventById(EventId1, Email);

        Response<FullEventDTO> expected = GetEventByIdResponse_EventFound();
        eventRepositoryMock.Verify(e => e.GetEventById(OwnerId, Email, EventId1), Times.Once());
        userRepositoryMock.Verify(s => s.FindByEmail(Email), Times.Once());
        Assert.AreEqual(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual));
    }

    [Test]
    public void GetAllEvent_ShouldSuccess()
    {
        User testUser = GetTestUser();
        eventRepositoryMock.Setup(e => e.GetAllEvents(OwnerId, Email)).Returns(new List<Event>(){ GetTestEvent1(), GetTestEvent2() });
        userRepositoryMock.Setup(u => u.FindByEmail(Email)).Returns(testUser);

        Response<List<EventDTO>> actual = eventService.GetAllEvent(Email);

        Response<List<EventDTO>> expected = GetAllEventResponse();
        eventRepositoryMock.Verify(e => e.GetAllEvents(OwnerId, Email), Times.Once());
        userRepositoryMock.Verify(s => s.FindByEmail(Email), Times.Once());
        Assert.AreEqual(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual));
    }

    [Test]
    public void GetTodayEvent_ShouldSuccess()
    {
        User testUser = GetTestUser();
        userRepositoryMock.Setup(u => u.FindByEmail(Email)).Returns(testUser);
        eventRepositoryMock.Setup(e => e.GetTodayEventByOwnerId(testUser.Id)).Returns(new List<Event>() { GetTestEvent1(), GetTestEvent2() });

        Response<List<EventDTO>> actual = eventService.GetTodayEvent(Email);

        Response<List<EventDTO>> expected = GetTodayEventResponse();
        userRepositoryMock.Verify(u => u.FindByEmail(Email), Times.Once());
        eventRepositoryMock.Verify(e => e.GetTodayEventByOwnerId(testUser.Id), Times.Once());
        Assert.AreEqual(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual));
    }

    [Test]
    public void UpdateEvent_EventNotFound_ShouldSuccess()
    {
        eventRepositoryMock.Setup(e => e.GetById(EventId1)).Returns((Event)null);
        eventRepositoryMock.Setup(e => e.Update(It.IsAny<Event>())).Verifiable();

        Response<string> actual = eventService.UpdateEvent(GetUpdateEventRequestDTO(), EventId1);

        Response<string> expected = GetUpdateEventResponse_EventNotFound();
        eventRepositoryMock.Verify(e => e.GetById(EventId1), Times.Once());
        eventRepositoryMock.Verify(e => e.Update(It.IsAny<Event>()), Times.Never());
        Assert.AreEqual(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual));
    }

    [Test]
    public void UpdateEvent_EventFound_ShouldSuccess()
    {
        Event testEvent = GetTestEvent1();
        eventRepositoryMock.Setup(e => e.GetById(EventId1)).Returns(testEvent);
        eventRepositoryMock.Setup(e => e.Update(It.IsAny<Event>())).Verifiable();

        Response<string> actual = eventService.UpdateEvent(GetUpdateEventRequestDTO(), EventId1);

        Response<string> expected = GetUpdateEventResponse_EventFound();
        eventRepositoryMock.Verify(e => e.GetById(EventId1), Times.Once());
        eventRepositoryMock.Verify(e => e.Update(It.IsAny<Event>()), Times.Once());
        Assert.AreEqual(EventId1, testEvent.Id);
        Assert.AreEqual(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual));
    }

    [Test]
    public void DeleteEvent_EventNotFound_ShouldSuccess()
    {
        eventRepositoryMock.Setup(e => e.GetById(EventId1)).Returns((Event)null);
        eventRepositoryMock.Setup(e => e.Remove(It.IsAny<Event>())).Verifiable();

        Response<string> actual = eventService.DeleteEvent(EventId1);

        Response<string> expected = GetDeleteEventResponse_EventNotFound();
        eventRepositoryMock.Verify(e => e.GetById(EventId1), Times.Once());
        eventRepositoryMock.Verify(e => e.Remove(It.IsAny<Event>()), Times.Never());
        Assert.AreEqual(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual));
    }

    [Test]
    public void DeleteEvent_EventFound_ShouldSuccess()
    {
        Event testEvent = GetTestEvent1();
        eventRepositoryMock.Setup(e => e.GetById(EventId1)).Returns(testEvent);
        eventRepositoryMock.Setup(e => e.Remove(It.IsAny<Event>())).Verifiable();

        Response<string> actual = eventService.DeleteEvent(EventId1);

        Response<string> expected = GetDeleteEventResponse_EventFound();
        eventRepositoryMock.Verify(e => e.GetById(EventId1), Times.Once());
        eventRepositoryMock.Verify(e => e.Remove(It.IsAny<Event>()), Times.Once());
        Assert.AreEqual(EventId1, testEvent.Id);
        Assert.AreEqual(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual));
    }

    private Event GetTestEvent1()
    {
        return new Event
        {
            Id = EventId1,
            Title = Title,
            StartDateTime = StartDateTime,
            EndDateTime = EndDateTime,
            Location = Location,
            Reminder = Reminder,
            Guests = Guests,
            Color = Color,
            Description = Description,
            OwnerId = OwnerId
        };
    }

    private Event GetTestEvent2()
    {
        return new Event
        {
            Id = EventId2,
            Title = Title,
            StartDateTime = StartDateTime.AddDays(-1),
            EndDateTime = EndDateTime,
            Location = Location,
            Reminder = Reminder,
            Guests = Guests,
            Color = Color,
            Description = Description,
            OwnerId = OwnerId
        };
    }

    private EventDTO GetEventDTO1()
    {
        return new EventDTO
        {
            Id = EventId1,
            Title = Title,
            StartDateTime = StartDateTime.ToString("M/d/yyyy h:mm:ss tt"),
            EndDateTime = EndDateTime.ToString("M/d/yyyy h:mm:ss tt"),
            Reminder = Reminder.ToString(),
            Color = Color,
        };
    }

    private EventDTO GetEventDTO2()
    {
        return new EventDTO
        {
            Id = EventId2,
            Title = Title,
            StartDateTime = StartDateTime.AddDays(-1).ToString("M/d/yyyy h:mm:ss tt"),
            EndDateTime = EndDateTime.ToString("M/d/yyyy h:mm:ss tt"),
            Reminder = Reminder.ToString(),
            Color = Color,
        };
    }

    private User GetTestUser()
    {
        return new User
        {
            Id = OwnerId,
            Email = Email,
            Password = Password,
            FullName = FullName,
            Phone = Phone
        };
    }

    private AddEventRequestDTO GetAddEventRequestDTO()
    {
        return new AddEventRequestDTO
        {
            Title = Title,
            StartDateTime = StartDateTime.ToString("MM/dd/yyyy h:mm:ss tt"),
            EndDateTime = EndDateTime.ToString("MM/dd/yyyy h:mm:ss tt"),
            Location = Location,
            Reminder = Reminder.ToString(),
            Guests = Guests,
            Color = Color,
            Description = Description,
        };
    }

    private AddEventRequestDTO GetUpdateEventRequestDTO()
    {
        return new AddEventRequestDTO
        {
            Title = Title,
            StartDateTime = StartDateTime.ToString("M/d/yyyy h:mm:ss tt"),
            EndDateTime = EndDateTime.ToString("M/d/yyyy h:mm:ss tt"),
            Location = Location,
            Reminder = Reminder.ToString(),
            Guests = Guests,
            Color = Color,
            Description = Description,
        };
    }

    private Response<string> GetAddEventResponse()
    {
        return new Response<string>
        {
            Code = 200,
            Status = "Success",
            Data = null,
            Message = "Event successfully created!"
        };
    }

    private Response<FullEventDTO> GetEventByIdResponse_EventNotFound()
    {
        return new Response<FullEventDTO>
        {
            Code = 400,
            Status = "Error",
            Data = null,
            Message = "Event not found!"
        };
    }

    private Response<FullEventDTO> GetEventByIdResponse_EventFound()
    {
        return new Response<FullEventDTO>
        {
            Code = 200,
            Status = "OK",
            Data = GetFullEventDTO(),
            Message = "Event found!"
        };
    }

    private FullEventDTO GetFullEventDTO()
    {
        return new FullEventDTO
        {
            Id = EventId1,
            Title = Title,
            StartDateTime = StartDateTime.ToString("M/d/yyyy h:mm:ss tt"),
            EndDateTime = EndDateTime.ToString("M/d/yyyy h:mm:ss tt"),
            Location = Location,
            Reminder = Reminder.ToString(),
            Guests = Guests,
            Color = Color,
            Description = Description,
        };
    }

    private Response<List<EventDTO>> GetAllEventResponse()
    {
        return new Response<List<EventDTO>>
        {
            Code = 200,
            Status = "OK",
            Data = new List<EventDTO> { GetEventDTO2(), GetEventDTO1() },
            Message = null
        };
    }

    private Response<List<EventDTO>> GetTodayEventResponse()
    {
        return new Response<List<EventDTO>>
        {
            Code = 200,
            Status = "OK",
            Data = new List<EventDTO> { GetEventDTO2(), GetEventDTO1() },
            Message = null
        };
    }

    private Response<string> GetUpdateEventResponse_EventNotFound()
    {
        return new Response<string>
        {
            Code = 400,
            Status = "Error",
            Data = null,
            Message = "Event is not exist!"
        };
    }

    private Response<string> GetUpdateEventResponse_EventFound()
    {
        return new Response<string>
        {
            Code = 200,
            Status = "OK",
            Data = null,
            Message = String.Format("Event with id: {0} is succesfully updated!", EventId1)
        };
    }

    private Response<string> GetDeleteEventResponse_EventNotFound()
    {
        return new Response<string>
        {
            Code = 400,
            Status = "Error",
            Data = null,
            Message = "Event is not exist!"
        };
    }

    private Response<string> GetDeleteEventResponse_EventFound()
    {
        return new Response<string>
        {
            Code = 200,
            Status = "OK",
            Data = null,
            Message = String.Format("Event with id: {0} is succesfully removed!", EventId1)
        };
    }
}