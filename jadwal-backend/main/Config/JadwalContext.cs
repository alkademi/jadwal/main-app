﻿using jadwal_backend.Models;
using Microsoft.EntityFrameworkCore;

namespace jadwal_backend.Config
{
    public class JadwalContext : DbContext
    {
        public JadwalContext(DbContextOptions<JadwalContext> options): base(options) { }
        public DbSet<User>? Users { get; set; }
        public DbSet<Event>? Events { get; set; }
    }
}
