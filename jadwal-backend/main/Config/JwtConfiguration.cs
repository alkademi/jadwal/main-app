﻿namespace jadwal_backend.Config
{
    public class JwtConfiguration
    {
        public string? Key;
        public string? Issuer;
    }
}
