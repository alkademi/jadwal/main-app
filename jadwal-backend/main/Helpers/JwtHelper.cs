﻿using System.IdentityModel.Tokens.Jwt;

namespace jadwal_backend.Helpers
{
    public static class JwtHelper
    {
        public static string ExtractEmail(string rawToken)
        {
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken token = handler.ReadJwtToken(rawToken);
            return token.Claims.First(claim => claim.Type == "email").Value;
        }
    }
}
