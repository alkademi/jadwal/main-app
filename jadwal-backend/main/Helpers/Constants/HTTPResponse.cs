﻿namespace jadwal_backend.Helpers.Constants
{
    public static class HTTPResponse
    {
        public const string REQUEST_INVALID = "Request is invalid";
        public const string EMAIL_INVALID = "Email is invalid";
        public const string USER_CREATED = "User successfully created";
        public const string USER_ALREADY_EXIST = "User already exist";
        public const string PASSWORD_LESS_THAN_SIX_CHAR = "Password should be at least 6 characters";
        public const string PHONE_INVALID = "Phone number is invalid";
        public const string FULL_NAME_NULL = "Full name should not be null";
    }
}
