﻿using jadwal_backend.Models.DTO;
using jadwal_backend.Models.DTO.Requests;
using jadwal_backend.Models;
using System.Globalization;

namespace jadwal_backend.Helpers
{
    public static class EventHelper
    {
        public static Event toEvent(AddEventRequestDTO eventDTO)
        {
            return new Event
            {
                Title = eventDTO.Title,
                StartDateTime = DateTime.ParseExact(eventDTO.StartDateTime,"M/d/yyyy h:mm:ss tt",CultureInfo.InvariantCulture),
                EndDateTime = DateTime.ParseExact(eventDTO.EndDateTime, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture),
                Location = eventDTO.Location,
                Reminder = TimeSpan.Parse(eventDTO.Reminder),
                Guests = eventDTO.Guests,
                Color = eventDTO.Color,
                Description = eventDTO.Description
            };
        }

        public static EventDTO toEventDTO(Event eventValue)
        {
            return new EventDTO
            {
                Id = eventValue.Id,
                Title = eventValue.Title,
                StartDateTime = eventValue.StartDateTime.ToString("M/d/yyyy h:mm:ss tt"),
                EndDateTime = eventValue.EndDateTime.ToString("M/d/yyyy h:mm:ss tt"),
                Reminder = eventValue.Reminder.ToString(),
                Color = eventValue.Color
            };
        }

        public static FullEventDTO toFullEventDTO(Event eventValue)
        {
            return new FullEventDTO
            {
                Id = eventValue.Id,
                Title = eventValue.Title,
                StartDateTime = eventValue.StartDateTime.ToString("M/d/yyyy h:mm:ss tt"),
                EndDateTime = eventValue.EndDateTime.ToString("M/d/yyyy h:mm:ss tt"),
                Location = eventValue.Location.ToString(),
                Reminder = eventValue.Reminder.ToString(),
                Guests = eventValue.Guests,
                Color = eventValue.Color,
                Description = eventValue.Description
            };
        }
    }
}
