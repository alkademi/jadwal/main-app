﻿using jadwal_backend.Models;

namespace jadwal_backend.Repositories
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User FindByEmail(string email);

        void Update(User user);
    }
}
