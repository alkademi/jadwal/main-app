﻿using jadwal_backend.Models;

namespace jadwal_backend.Repositories
{
    public interface IEventRepository : IBaseRepository<Event>
    {
        public void Update(Event val);

        public Event GetEventById(int ownerId, string userEmail, int id);

        public List<Event> GetAllEvents(int ownerId, string userEmail);
        public List<Event> GetByDate(int ownerId, string userEmail, DateTime date);

        public List<Event> GetTodayEventByOwnerId(int ownerId);
    }
}
