﻿using jadwal_backend.Config;
using jadwal_backend.Models;

namespace jadwal_backend.Repositories.Impl
{
    public class EventRepository : BaseRepository<Event>, IEventRepository
    {
        public EventRepository(JadwalContext context) : base(context)
        {
        }

        public void Update(Event val)
        {
            _context.Update(val);
            _context.SaveChanges();
        }

        public Event GetEventById(int ownerId, string userEmail, int id)
        {
            return base.Find(e => e.Id == id && (e.OwnerId == ownerId || e.Guests.ToLower().Contains(userEmail.ToLower()))).FirstOrDefault();
        }

        public List<Event> GetAllEvents(int ownerId, string userEmail)
        {
            return base.Find(e => (e.OwnerId == ownerId || e.Guests.ToLower().Contains(userEmail.ToLower()))).ToList();
        }

        public List<Event> GetByDate(int ownerId, string userEmail, DateTime date)
        {
            return base.Find(e => e.StartDateTime.Date <= date && e.StartDateTime.Date.AddDays(1).AddSeconds(-1) >= date
                && (e.OwnerId == ownerId || e.Guests.ToLower().Contains(userEmail.ToLower()))).ToList();
        }

        public List<Event> GetTodayEventByOwnerId(int ownerId)
        {
            DateTime date = DateTime.Today;
            return base.Find(e => e.StartDateTime.Date == date && e.StartDateTime.Date.AddDays(1).AddSeconds(-1) >= date && e.OwnerId == ownerId).ToList();
        }
    }
}
