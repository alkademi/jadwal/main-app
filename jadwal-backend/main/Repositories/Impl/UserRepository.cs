﻿using jadwal_backend.Models;
using jadwal_backend.Config;
using Microsoft.EntityFrameworkCore;

namespace jadwal_backend.Repositories.Impl
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(JadwalContext jadwalContext) : base(jadwalContext)
        {
        }

        public User FindByEmail(string email)
        {
            return base.Find(e => e.Email == email).FirstOrDefault();
        }

        public void Update(User user)
        {
            _context.Update(user);
            _context.SaveChanges();
        }
    }
}
