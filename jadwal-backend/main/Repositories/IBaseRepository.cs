﻿using System.Linq.Expressions;

namespace jadwal_backend.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        T GetById(int id);
        IEnumerable<T> GetAll();
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);
        void Add(T entity);
        void AddAll(IEnumerable<T> entities);
        void Remove(T entity);
        void RemoveAll(IEnumerable<T> entities);
    }
}
