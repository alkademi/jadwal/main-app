# Jadwal Backend

Backend for Jadwal.id.

## Installation

- Install these NuGet packages using cmd.

```bash
dotnet tool install --global dotnet-ef
dotnet add package Microsoft.EntityFrameworkCore --version 6.0.0
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL --version 6.0.0
dotnet add package Microsoft.EntityFrameworkCore.Design --version 6.0.0
dotnet add package Microsoft.AspNetCore.Diagnostics.EntityFrameworkCore --version 6.0.0
dotnet add package BCrypt.Net-Next --version 4.0.2

dotnet dev-certs https --trust	//for accepting connection from local
```

- Create a new database in your local postgres. Change ```appsetings.json``` ```DefaultConnection``` to match your database

- Migrate the database with this command. Do this everytime you add/delete column/table in the database
```bash
dotnet ef migrations list --project jadwal-backend.csproj
dotnet ef database update --project jadwal-backend.csproj
```

## Usage

- Run the project

```bash
dotnet run --project jadwal-backend.csproj
```

- Open the swagger to try the API

```bash
https://localhost:7038/swagger/index.html
```

## Note
Service ini menggunakan outbound service dari https://kalenderindonesia.com/api. Dengan API token yang digunakan merupakan API token salah satu anggota kami (Tito 13519007). Sehingga untuk pengembangan selanjutnya dapat diubah terlebih dahulu API token tersebut agar tidak lagi menggunakan API token anggota kami. Selain itu, dalam melakukan deployment staging kami menggunakan Heroku sehingga jika ingin melanjutkan apa yang telah kami lakukan, teman-teman developer dapat melakukan set up Heroku app dan variable gitlab untuk melanjutkan deployment (dapat melihat .gitlab-ci.yml untuk mengetahui variable apa saja yang perlu disimpan).
