﻿using jadwal_backend.Services;
using jadwal_backend.Models;
using jadwal_backend.Models.DTO;
using jadwal_backend.Models.DTO.Requests;
using jadwal_backend.Models.DTO.Responses;
using jadwal_backend.Outbounds;
using jadwal_backend.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Primitives;

namespace jadwal_backend.Controllers
{
    [ApiController]
    [Route("event")]
    public class EventController : ControllerBase
    {

        private readonly IEventService? _eventService;
        private readonly ICalendarOutboundService? _calendarOutboundService;

        public EventController(IEventService eventService, ICalendarOutboundService calendarOutboundService)
        {
            _eventService = eventService;
            _calendarOutboundService = calendarOutboundService;
        }

        [HttpPost("add")]
        [Authorize]
        public Response<string> AddEvent(AddEventRequestDTO request)
        {
            Request.Headers.TryGetValue("Authorization", out StringValues rawToken);
            string token = rawToken.ToString();
            token = token.Split(' ')[1];
            string email = JwtHelper.ExtractEmail(token);

            return _eventService.AddEvent(request, email);
        }

        [HttpGet("{id:int}")]
        [Authorize]
        public Response<FullEventDTO> GetEventById(int id)
        {
            Request.Headers.TryGetValue("Authorization", out StringValues rawToken);
            string token = rawToken.ToString();
            token = token.Split(' ')[1];
            string email = JwtHelper.ExtractEmail(token);

            return _eventService.GetEventById(id, email);
        }

        [HttpGet]
        [Authorize]
        public Response<List<EventDTO>> GetAllEvent()
        {
            Request.Headers.TryGetValue("Authorization", out StringValues rawToken);
            string token = rawToken.ToString();
            token = token.Split(' ')[1];
            string email = JwtHelper.ExtractEmail(token);

            return _eventService.GetAllEvent(email);
        }

        [HttpGet("today")]
        [Authorize]
        public Response<List<EventDTO>> GetTodayEvent()
        {
            Request.Headers.TryGetValue("Authorization", out StringValues rawToken);
            string token = rawToken.ToString();
            token = token.Split(' ')[1];
            string email = JwtHelper.ExtractEmail(token);

            return _eventService.GetTodayEvent(email);
        }

        [HttpPut("update/{id:int}")]
        [Authorize]
        public Response<string> UpdateEvent(AddEventRequestDTO request, int id)
        {
            return _eventService.UpdateEvent(request, id);
        }

        [HttpDelete("delete/{id:int}")]
        [Authorize]
        public Response<string> DeleteEvent(int id)
        {
            return _eventService.DeleteEvent(id);
        }

    }
}
