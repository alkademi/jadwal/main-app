using jadwal_backend.Helpers;
using jadwal_backend.Models;
using jadwal_backend.Models.DTO.Requests;
using jadwal_backend.Models.DTO.Responses;
using jadwal_backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace jadwal_backend.Controllers
{
    [ApiController]
    [Route("user")]
    public class UserAccountController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserAccountController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("register")]
        public Response<string> Register(RegisterRequestDTO request)
        {
            return _userService.Register(request);
        }

        [HttpPost("login")]
        public Response<string> Login(LoginRequestDTO req)
        {
            return _userService.Login(req);
        }

        [HttpPut("edit")]
        [Authorize]
        public Response<string> EditProfile(EditProfileRequestDTO req)
        {
            Request.Headers.TryGetValue("Authorization", out StringValues rawToken);
            string token = rawToken.ToString();
            token = token.Split(' ')[1];
            string email = JwtHelper.ExtractEmail(token);

            return _userService.Edit(req, email);
        }

        [HttpGet("detail")]
        [Authorize]
        public Response<User> GetDetail()
        {
            Request.Headers.TryGetValue("Authorization", out StringValues rawToken);
            string token = rawToken.ToString();
            token = token.Split(' ')[1];
            string email = JwtHelper.ExtractEmail(token);

            return _userService.GetDetail(email);
        }
    }
}
