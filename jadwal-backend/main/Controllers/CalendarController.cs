﻿using jadwal_backend.Models.DTO;
using jadwal_backend.Services;
using jadwal_backend.Models.DTO.Responses;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Primitives;
using jadwal_backend.Helpers;

namespace jadwal_backend.Controllers
{
    [ApiController]
    [Route("calendar")]
    public class CalendarController : Controller
    {

        private readonly ICalendarService? _calendarService;
        public CalendarController(ICalendarService calendarService)
        {
            _calendarService = calendarService;
        }

        [HttpGet("{year:int}/{month:int}")]
        [Authorize]
        public Response<List<DateDTO>> GetMonth(int year, int month)
        {
            Request.Headers.TryGetValue("Authorization", out StringValues rawToken);
            string token = rawToken.ToString();
            token = token.Split(' ')[1];
            string email = JwtHelper.ExtractEmail(token);

            return _calendarService.GetMonth(email, year, month).Result;
        }
    }
}
