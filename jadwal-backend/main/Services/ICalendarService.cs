﻿using jadwal_backend.Models.DTO;
using jadwal_backend.Models.DTO.Responses;

namespace jadwal_backend.Services
{
    public interface ICalendarService
    {
        public Task<Response<List<DateDTO>>> GetMonth(string email, int year, int month);
    }
}
