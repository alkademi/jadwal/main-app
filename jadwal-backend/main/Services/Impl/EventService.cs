﻿using jadwal_backend.Models.DTO;
using jadwal_backend.Models.DTO.Requests;
using jadwal_backend.Models.DTO.Responses;
using jadwal_backend.Models;
using jadwal_backend.Helpers;
using jadwal_backend.Repositories;
using System.Globalization;

namespace jadwal_backend.Services.Impl
{
    public class EventService : IEventService
    {
        private readonly IEventRepository _repository;
        private readonly IUserRepository _userRepository;
        public EventService(IEventRepository repository, IUserRepository userRepository)
        {
            _repository = repository;
            _userRepository = userRepository;
        }

        public Response<string> AddEvent(AddEventRequestDTO requestDTO, string email)
        {
            // Console.WriteLine(requestDTO.StartDateTime);
            Event valEvent = EventHelper.toEvent(requestDTO);
            // Console.WriteLine(valEvent.StartDateTime.Day);
            // Console.WriteLine(valEvent.StartDateTime.Month);
            User ownerEmail = _userRepository.FindByEmail(email);
            valEvent.OwnerId = ownerEmail.Id;
            _repository.Add(valEvent);
            
            return new Response<string>
            {
                Code = 200,
                Status = "Success",
                Data = null,
                Message = "Event successfully created!"
            };
        }

        public Response<FullEventDTO> GetEventById(int id, string email)
        {
            User user = _userRepository.FindByEmail(email);
            Event valEvent = _repository.GetEventById(user.Id, user.Email, id);
            if (valEvent == null)
            {
                return new Response<FullEventDTO>
                {
                    Code = 400,
                    Status = "Error",
                    Data = null,
                    Message = "Event not found!"
                };
            }
            // Console.WriteLine(valEvent.StartDateTime.ToString());
            // Console.WriteLine(valEvent.StartDateTime.Day);
            // Console.WriteLine(valEvent.StartDateTime.Month);
            FullEventDTO retval = EventHelper.toFullEventDTO(valEvent);
            return new Response<FullEventDTO>
            {
                Code = 200,
                Status = "OK",
                Data = retval,
                Message = "Event found!"
            };
        }

        public Response<List<EventDTO>> GetAllEvent(string email)
        {
            User user = _userRepository.FindByEmail(email);
            List<Event> events = (List<Event>)_repository.GetAllEvents(user.Id, user.Email);
            events.Sort();

            List<EventDTO> data = new List<EventDTO>();
            foreach (Event val in events)
            {
                EventDTO tempEvent = EventHelper.toEventDTO(val);
                data.Add(tempEvent);
            }

            return new Response<List<EventDTO>>
            {
                Code = 200,
                Status = "OK",
                Data = data,
                Message = null
            };
        }

        public Response<List<EventDTO>> GetTodayEvent(string email)
        {
            User owner = _userRepository.FindByEmail(email);
            List<Event> events = _repository.GetTodayEventByOwnerId(owner.Id);
            events.Sort();

            List<EventDTO> data = new List<EventDTO>();
            foreach (Event val in events)
            {
                EventDTO tempEvent = EventHelper.toEventDTO(val);
                data.Add(tempEvent);
            }

            return new Response<List<EventDTO>>
            {
                Code = 200,
                Status = "OK",
                Data = data,
                Message = null
            };
        }

        public Response<string> UpdateEvent(AddEventRequestDTO requestDTO, int id)
        {
            Event valEvent = _repository.GetById(id);
            if (valEvent == null)
            {
                return new Response<string>
                {
                    Code = 400,
                    Status = "Error",
                    Data = null,
                    Message = "Event is not exist!"
                };
            }

            valEvent.Title = requestDTO.Title;
            valEvent.StartDateTime = DateTime.ParseExact(requestDTO.StartDateTime,"M/d/yyyy h:mm:ss tt",CultureInfo.InvariantCulture);
            valEvent.EndDateTime = DateTime.ParseExact(requestDTO.EndDateTime,"M/d/yyyy h:mm:ss tt",CultureInfo.InvariantCulture);
            valEvent.Location = requestDTO.Location;
            valEvent.Reminder = TimeSpan.Parse(requestDTO.Reminder);
            valEvent.Guests = requestDTO.Guests;
            valEvent.Color = requestDTO.Color;
            valEvent.Description = requestDTO.Description;
            
            _repository.Update(valEvent);
            return new Response<string>
            {
                Code = 200,
                Status = "OK",
                Data = null,
                Message = String.Format("Event with id: {0} is succesfully updated!", id)
            };
        }

        public Response<string> DeleteEvent(int id)
        {
            Event valEvent = _repository.GetById(id);
            if (valEvent == null)
            {
                return new Response<string>
                {
                    Code = 400,
                    Status = "Error",
                    Data = null,
                    Message = "Event is not exist!"
                };
            }

            _repository.Remove(valEvent);
            return new Response<string>
            {
                Code = 200,
                Status = "OK",
                Data = null,
                Message = String.Format("Event with id: {0} is succesfully removed!", id)
            };
        }

    }
}
