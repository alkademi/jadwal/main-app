﻿using jadwal_backend.Models.DTO.Requests;
using jadwal_backend.Models.DTO.Responses;
using jadwal_backend.Models;
using jadwal_backend.Repositories;
using jadwal_backend.Helpers.Constants;
using jadwal_backend.Config;

using System.Text.RegularExpressions;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace jadwal_backend.Services.Impl
{
    public class UserService: IUserService
    {
        private readonly IConfiguration _jwtConfiguration;
        IUserRepository _repository;
        public UserService(IUserRepository userRepository, IConfiguration jwtConfiguration)
        {
            _repository = userRepository;
            _jwtConfiguration = jwtConfiguration;
        }

        public Response<string> Login(LoginRequestDTO request)
        {
            User acc = _repository.FindByEmail(request.Email);
            if (acc == null)
            {
                return new Response<string>
                {
                    Code = 400,
                    Status = "Error",
                    Data = null,
                    Message = "Invalid Email!"
                };
            }

            if (VerifyPassword(request.Password, acc.Password))
            {
                return new Response<string>
                {
                    Code = 200,
                    Status = "OK",
                    Data = GenerateJSONWebToken(acc),
                    Message = "Login success!"
                };
            }

            return new Response<string>
            {
                Code = 400,
                Status = "Error",
                Data = null,
                Message = "Password is not matched!"
            };
        }

        public Response<string> Register(RegisterRequestDTO request)
        {
            Response<string> response = new Response<string>
            {
                Code = 400,
                Status = "Error",
                Data = null
            };

            if (!IsValidEmail(request.Email))
            {
                response.Message = HTTPResponse.EMAIL_INVALID;
                return response;
            }

            if (request.FullName.Length == 0 || request.FullName == null)
            {
                response.Message = HTTPResponse.FULL_NAME_NULL;
                return response;
            }

            if (!IsValidPhone(request.Phone))
            {
                response.Message = HTTPResponse.PHONE_INVALID;
                return response;
            }

            if (request.Password.Length < 6)
            {
                response.Message = HTTPResponse.PASSWORD_LESS_THAN_SIX_CHAR;
                return response;
            }

            User user = _repository.FindByEmail(request.Email);
            if (user != null)
            {
                response.Message =HTTPResponse.USER_ALREADY_EXIST;
                return response;
            };

            User newUser = new()
            {
                Email = request.Email,
                Phone = request.Phone,
                Password = HashPassword(request.Password),
                FullName = request.FullName,
            };
            _repository.Add(newUser);

            response.Code = 200;
            response.Status = "OK";
            response.Data = GenerateJSONWebToken(newUser);
            response.Message = HTTPResponse.USER_CREATED;

            return response;
        }

        public Response<string> Edit(EditProfileRequestDTO request, string email)
        {
            Response<string> response = new Response<string>
            {
                Code = 400,
                Status = "Error",
                Data = null
            };

            User user = _repository.FindByEmail(email);
            if (user != null)
            {
                if (request.FullName.Length == 0 || request.FullName == null)
                {
                    response.Message = HTTPResponse.FULL_NAME_NULL;
                    return response;
                }

                if (!IsValidPhone(request.Phone))
                {
                    response.Message = HTTPResponse.PHONE_INVALID;
                    return response;
                }

                if (request.Password.Length < 6)
                {
                    response.Message = HTTPResponse.PASSWORD_LESS_THAN_SIX_CHAR;
                    return response;
                }

                if (request.Password != user.Password)
                {
                    user.Password = HashPassword(request.Password);
                }
                user.Phone = request.Phone;
                user.FullName = request.FullName;

                _repository.Update(user);

                response.Code = 200;
                response.Status = "OK";
                response.Message = "Update profile is succeed!";
                return response;
            };

            response.Code = 400;
            response.Status = "Error";
            response.Message = "User is not exist!";
            return response;
        }

        public Response<User> GetDetail(string email)
        {
            Response<User> response = new Response<User>
            {
                Data = null
            };

            User user = _repository.FindByEmail(email);
            response.Code = 200;
            response.Status = "OK";
            response.Message = "Get user profile succeed!";
            response.Data = user;
            return response;
        }

        private string GenerateJSONWebToken(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtConfiguration.GetValue<string>("Jwt:Key")));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Email, user.Email)
            };

            var token = new JwtSecurityToken(_jwtConfiguration.GetValue<string>("Jwt:Issuer"),
              _jwtConfiguration.GetValue<string>("Jwt:Issuer"),
              claims,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        private bool VerifyPassword(string password, string hashedPassword)
        {
            return BCrypt.Net.BCrypt.Verify(password, hashedPassword);
        }

        private bool IsValidEmail(string Email)
        {
            Email = Email == null ? "" : Email;
            string RegexMatcher = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            Regex Re = new Regex(RegexMatcher, RegexOptions.IgnoreCase);

            if (Re.IsMatch(Email))
            {
                return true;
            }
            return false;
        }

        private bool IsValidPhone(string Phone)
        {
            Phone = Phone == null ? "" : Phone;
            string RegexMatcher = "\\(?\\d{3}\\)?-? *\\d{3}-? *-?\\d{4}";
            Regex Re = new Regex(RegexMatcher, RegexOptions.IgnoreCase);

            if (Re.IsMatch(Phone))
            {
                return true;
            }
            return false;
        }
    }
}
