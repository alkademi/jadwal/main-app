﻿using jadwal_backend.Models.DTO;
using jadwal_backend.Models.DTO.Responses;
using jadwal_backend.Models;
using jadwal_backend.Outbounds;
using jadwal_backend.Repositories;
using jadwal_backend.Helpers;
using System.Globalization;

namespace jadwal_backend.Services.Impl
{
    public class CalendarService : ICalendarService
    {
        private readonly IEventRepository _eventRepository;
        private readonly IUserRepository _userRepository;
        private readonly ICalendarOutboundService _calendarOutboundService;

        public CalendarService(IEventRepository eventRepository, IUserRepository userRepository, ICalendarOutboundService calendarOutboundService)
        {
            _eventRepository = eventRepository;
            _userRepository = userRepository;
            _calendarOutboundService = calendarOutboundService;
        }

        public async Task<Response<List<DateDTO>>> GetMonth(string email, int year, int month)
        {
            User user = _userRepository.FindByEmail(email);
            Task<MonthlyResponse> calendarAPITask = _calendarOutboundService.GetMonthly(year, month);
            MonthlyResponse calendarAPIResponse = await calendarAPITask;

            List<DateDTO> result = new List<DateDTO>();
            foreach (MonthlyResponse.Daily daily in calendarAPIResponse.Data.Monthly.Daily)
            {
                DateDTO resultDate = new DateDTO();

                DateTime dailyDate = DateTime.ParseExact(daily.Date.M, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                resultDate.Status = daily.Type;
                resultDate.Date = dailyDate.Day;
                resultDate.Day = daily.Date.M;

                List<Event> events = _eventRepository.GetByDate(user.Id, user.Email, dailyDate);
                if (events != null)
                {
                    List<EventDTO> briefEvents = new();
                    foreach (Event e in events)
                    {
                        EventDTO briefEvent = EventHelper.toEventDTO(e);
                        briefEvents.Add(briefEvent);
                    }

                    resultDate.Events = briefEvents;
                }

                DateDTO.BriefHoliday briefHoliday = new DateDTO.BriefHoliday();
                if (daily.Holiday != null)
                {
                    briefHoliday.IsHoliday = true;
                    briefHoliday.Title = daily.Holiday[0].Name;
                }

                resultDate.Holiday = briefHoliday;

                result.Add(resultDate);
            }

            Console.WriteLine(result.Count);
            return new Response<List<DateDTO>>
            {
                Code = 200,
                Status = "OK",
                Data = result,
                Message = null
            };
        }
    }
}
