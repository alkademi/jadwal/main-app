﻿using jadwal_backend.Models.DTO;
using jadwal_backend.Models.DTO.Responses;
using jadwal_backend.Models.DTO.Requests;
using jadwal_backend.Models;

namespace jadwal_backend.Services
{
    public interface IEventService
    {
        public Response<string> AddEvent(AddEventRequestDTO requestDTO, string email);

        public Response<FullEventDTO> GetEventById(int id, string email);

        public Response<List<EventDTO>> GetAllEvent(string email);

        public Response<List<EventDTO>> GetTodayEvent(string email);

        public Response<string> UpdateEvent(AddEventRequestDTO requestDTO, int id);

        public Response<string> DeleteEvent(int id);
    }
}
