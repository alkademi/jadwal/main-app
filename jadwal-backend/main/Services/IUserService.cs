﻿using jadwal_backend.Models;
using jadwal_backend.Models.DTO.Requests;
using jadwal_backend.Models.DTO.Responses;

namespace jadwal_backend.Services
{
    public interface IUserService
    {
        public Response<string> Register(RegisterRequestDTO request);

        public Response<string> Login(LoginRequestDTO request);

        public Response<string> Edit(EditProfileRequestDTO request, string email);

        public Response<User> GetDetail(string email);
    }
}
