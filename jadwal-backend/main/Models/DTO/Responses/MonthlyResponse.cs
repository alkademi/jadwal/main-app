﻿namespace jadwal_backend.Models.DTO.Responses
{
    public class MonthlyResponse
    {
        int Success { get; set; }

        public MonthlyData? Data { get; set; }

        public MonthlyResponse()
        {

        }

        //public override string ToString()
        //{
        //    string retVal = Success.ToString();
        //    retVal += Data.
        //    return base.ToString();
        //}

        public class MonthlyData
        {
            public int Year { get; set; }
            public int Month { get; set; }
            public Detail? Monthly { get; set; }

        }

        public class Detail
        {
            public int DaysCount { get; set; }
            public int WeeksCount { get; set; }
            public int FirstWeek { get; set; }
            public List<Daily>? Daily { get; set; }
        }

        public class Daily
        {
            public string? Type { get; set; }
            public Content? Date { get; set; }
            public Content? Text { get; set; }
            public List<Holiday>? Holiday { get; set; }
        }

        public class Content
        {
            public string? M { get; set; }
            public string? H { get; set; }
            public string? J { get; set; }
            public string? C { get; set; }
            public string? S { get; set; }
            public string? W { get; set; }
        }

        public class Holiday
        {
            public string? Date { get; set; }
            public int Day { get; set; }
            public string? Name { get; set; }
        }
    }
}
