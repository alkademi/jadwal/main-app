﻿using System.ComponentModel.DataAnnotations;

namespace jadwal_backend.Models.DTO.Responses
{
    public class Response<T>
    {
        [Required]
        public int? Code { get; set; }

        [Required]
        public string? Status { get; set; }

        public T? Data { get; set; }

        public string? Message { get; set; }
    }
}
