namespace jadwal_backend.Models.DTO
{
    public class FullEventDTO: EventDTO
    {
        public FullEventDTO(): base()
        {

        }

        public string? Location { get; set; }


        public string? Guests { get; set; }             


        public string? Description { get; set; }
    }
}
