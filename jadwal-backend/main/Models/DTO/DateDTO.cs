﻿namespace jadwal_backend.Models.DTO
{
    public class DateDTO
    {
        public string? Status { get; set; }
        public int Date { get; set; }
        public string? Day { get; set; }
        public List<EventDTO>? Events { get; set; }
        public BriefHoliday? Holiday { get; set; }

        public class BriefHoliday
        {
            public bool IsHoliday { get; set; }
            public string? Title { get; set; }
        }
    }
}
