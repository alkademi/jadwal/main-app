﻿using System.ComponentModel.DataAnnotations;

namespace jadwal_backend.Models.DTO.Requests
{
    public class LoginRequestDTO
    {
        [Required]
        public string? Email { get; set; }

        [Required]
        public string? Password { get; set; }
    }
}
