﻿using System.ComponentModel.DataAnnotations;

namespace jadwal_backend.Models.DTO.Requests
{
    public class AddEventRequestDTO
    {
        public AddEventRequestDTO()
        {

        }

        [Required]
        public string Title { get; set; }

        [Required]
        public string StartDateTime { get; set; }

        [Required]
        public string EndDateTime { get; set; }

        public string Location { get; set; }

        [Required]
        public string Reminder { get; set; }

        public string Guests { get; set; }

        [Required]
        public string Color { get; set; }

        public string Description { get; set; }
    }
}
