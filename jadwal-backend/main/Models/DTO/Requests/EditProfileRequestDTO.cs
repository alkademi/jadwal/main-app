﻿using System.ComponentModel.DataAnnotations;

namespace jadwal_backend.Models.DTO.Requests
{
    public class EditProfileRequestDTO
    {
        [Required]
        public string? FullName { get; set; }

        [Required]
        public string? Phone { get; set; }

        [Required]
        public string? Password { get; set; }
    }
}
