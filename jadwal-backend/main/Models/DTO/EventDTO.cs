﻿using System.ComponentModel.DataAnnotations;

namespace jadwal_backend.Models.DTO
{
    public class EventDTO
    {
        public EventDTO()
        {

        }

        [Required]
        public int Id { get; set; }

        [Required]
        public string? Title { get; set; }

        [Required]
        public string? StartDateTime { get; set; }

        [Required]
        public string? EndDateTime { get; set; }

        [Required]
        public string? Reminder { get; set; }

        [Required]
        public string? Color { get; set; }
    }
}
