using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace jadwal_backend.Models
{
    [Table("users")]
    public class User
    {
        public User(string email, string password)
        {
            Email = email; 
            Password = password;
        }

        public User()
        {
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("email")]
        public string Email { get; set; }

        [Required]
        [Column("password")]
        public string Password { get; set; }

        [Required]
        [Column("full_name")]
        public string FullName { get; set; }

        [Required]
        [Column("phone")]
        public string Phone { get; set; }
    }
}
