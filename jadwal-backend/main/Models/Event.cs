﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace jadwal_backend.Models
{
    [Table("events")]
    public class Event: IComparable<Event>
    {
        public Event()
        {
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("title")]
        public string? Title { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [Column("start_date_time")]
        public DateTime StartDateTime { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [Column("end_date_time")]
        public DateTime EndDateTime { get; set; }

        [Column("location")]
        public string? Location { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [Column("reminder")]
        public TimeSpan Reminder { get; set; }

        [Column("guests")]
        public string? Guests { get; set; }                     //list of email but in 1 string, separated by ','
        
        [Required]
        [Column("color")]
        public string? Color { get; set; }

        [Column("description")]
        public string? Description { get; set; }

        [Required]
        public int OwnerId { get; set; }

        public int CompareTo(Event? other)
        {
            // return other.Salary.CompareTo(this.Salary);
            if (this.StartDateTime < other?.StartDateTime)
            {
                return -1;
            }
            else if (this.StartDateTime > other?.StartDateTime)
            {
                return 1;
            }
            return 0;
        }
    }
}
