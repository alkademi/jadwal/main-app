﻿using System.Net.Http;

namespace jadwal_backend.Outbounds
{
    public static class Client
    {
        private static HttpClient _httpClient;
        public static HttpClient HttpClient
        {
            get
            {
                if (_httpClient == null)
                {
                    _httpClient = new HttpClient();            
                    return _httpClient;
                }
                return _httpClient;
            }
        }
    }
}
