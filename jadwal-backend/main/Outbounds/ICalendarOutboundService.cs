﻿using jadwal_backend.Models.DTO.Responses;
using System.Threading.Tasks;

namespace jadwal_backend.Outbounds
{
    public interface ICalendarOutboundService
    {
        public Task<MonthlyResponse> GetMonthly(int year, int month);
    }
}
