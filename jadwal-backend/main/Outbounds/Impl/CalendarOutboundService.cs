﻿using jadwal_backend.Models.DTO;
using jadwal_backend.Models.DTO.Responses;
using Newtonsoft.Json;

namespace jadwal_backend.Outbounds.Impl
{
    public class CalendarOutboundService : ICalendarOutboundService
    {
        private string BASE_PATH;
        private readonly IConfiguration _outboundConfiguration;
        private HttpClient _httpClient;

        public CalendarOutboundService(IConfiguration outboundConfiguration)
        {
            _httpClient = Client.HttpClient;
            _outboundConfiguration = outboundConfiguration;
            BASE_PATH = _outboundConfiguration.GetValue<string>("CalendarOutbound:Path") + _outboundConfiguration.GetValue<string>("CalendarOutbound:ApiKey") + "/"; 
        }

        public async Task<MonthlyResponse> GetMonthly(int year, int month)
        {
            try
            {
                string path = BASE_PATH + "kalender/masehi/" + year.ToString() + "/" + month.ToString();
                HttpResponseMessage response = await _httpClient.GetAsync(new Uri(path));
                if (response.IsSuccessStatusCode)
                {
                    string responseContent = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<MonthlyResponse>(responseContent);
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
                return null;
            }
        }
    }
}
