﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace jadwal_mobile
{
    public static class AppSettings
    {
        public static string SERVICE_BASE_PATH_DEVELOPMENT = 
            DeviceInfo.Platform == DevicePlatform.Android ? "https://10.0.2.2:7038/" : "https://localhost:7038/";
        public static string SERVICE_BASE_PATH_STAGING = "https://jadwal-backend.herokuapp.com/";
    }
}
