﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace jadwal_mobile.Behavior
{
    class LazyContentPageBehavior : LoadContentOnActivateBehavior<ContentPage>
    {
        protected override void SetContent(ContentPage element, View contentView)
        {
            element.Content = contentView;
        }
    }
}
