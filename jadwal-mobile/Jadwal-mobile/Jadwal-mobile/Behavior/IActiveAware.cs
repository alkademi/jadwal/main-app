﻿using System;
using System.Collections.Generic;
using System.Text;

namespace jadwal_mobile.Behavior
{
    interface IActiveAware
    {
        bool IsActive { get; set; }
        event EventHandler IsActiveChanged;
    }
}
