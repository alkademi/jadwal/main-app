﻿using jadwal_mobile.Models;
using jadwal_mobile.Models.Responses;

using System;
using System.Globalization;

namespace jadwal_mobile.Utils
{
    public static class EventHelper
    {
        public static Event ToEvent(EventResponse response)
        {
            Event retval = new Event();
            retval.Id = response.Id;
            retval.Title = response.Title;
            retval.StartDateTime = DateTime.ParseExact(response.StartDateTime, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            retval.EndDateTime = DateTime.ParseExact(response.EndDateTime, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            retval.Reminder = TimeSpan.Parse(response.Reminder);
            retval.Color = response.Color;

            return retval;
        }

        public static FullEvent ToFullEvent(FullEventResponse response)
        {
            FullEvent retval = new FullEvent();
            retval.Id = response.Id;
            retval.Title = response.Title;
            retval.StartDateTime = DateTime.ParseExact(response.StartDateTime, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            retval.EndDateTime = DateTime.ParseExact(response.EndDateTime, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            retval.Reminder = TimeSpan.Parse(response.Reminder);
            retval.Color = response.Color;
            retval.Location = response.Location;
            retval.Description = response.Description;
            retval.Guests = response.Guests;

            return retval;
        }
    }
}
