﻿using System;
using System.Net.Http;
using Xamarin.Essentials;

namespace jadwal_mobile.Utils
{
    public static class ConnectionHelper
    {
        public async static void SetTokenHeader(HttpClient httpClient)
        {
            string token = await SecureStorage.GetAsync("token");
            if (!httpClient.DefaultRequestHeaders.Contains("Authorization"))
            {
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            }
        }
    }
}
