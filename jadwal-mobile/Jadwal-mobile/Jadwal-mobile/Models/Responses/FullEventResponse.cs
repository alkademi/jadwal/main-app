﻿namespace jadwal_mobile.Models.Responses
{
    public class FullEventResponse: EventResponse
    {
        public string Location { get; set; }
        public string Guests { get; set; }
        public string Description { get; set; }
    }
}
