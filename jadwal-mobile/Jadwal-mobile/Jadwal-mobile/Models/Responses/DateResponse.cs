﻿using System.Collections.Generic;

namespace jadwal_mobile.Models.Responses
{
    public class DateResponse
    {
        public string Status { get; set; }
        public int Date { get; set; }
        public string Day { get; set; }
        public List<EventResponse> Events { get; set; }
        public Holiday Holiday { get; set; }
    }

    public class Holiday
    {
        public bool IsHoliday { get; set; }
        public string Title { get; set; }
    }
}
