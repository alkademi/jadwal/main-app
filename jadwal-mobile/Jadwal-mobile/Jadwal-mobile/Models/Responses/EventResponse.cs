﻿namespace jadwal_mobile.Models.Responses
{
    public class EventResponse
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public string Reminder { get; set; }
        public string Color { get; set; }
    }
}
