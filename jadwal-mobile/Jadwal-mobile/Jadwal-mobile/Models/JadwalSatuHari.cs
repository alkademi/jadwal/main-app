﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace jadwal_mobile.Models
{
    public class JadwalSatuHari
    {
        public int tanggal { get; set; }
        //public Image image { get; set; }
        public ObservableCollection<Jadwal> JadwalList { get; set; }
    }
}
