﻿using System;

namespace jadwal_mobile.Models
{
    public class FullEvent: Event
    {
        public string Location { get; set; }
        public string Guests { get; set; }          //list of email that with the delimiter ','
        public string Description { get; set; }
    }
}
