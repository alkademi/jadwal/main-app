﻿using System;

namespace jadwal_mobile.Models
{
    public class Event 
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public TimeSpan Reminder { get; set; }
        public string Color { get; set; }

        public string InformationTime
        {
            get
            {
                return string.Format("{0} - {1}", StartDateTime.ToString("HH:mm"), EndDateTime.ToString("HH:mm"));
            }
        }

        public string Duration
        {
            get
            {
                return string.Format("{0} - {1}", StartDateTime.ToString("HH:mm"), EndDateTime.ToString("HH:mm"));
            }
        }

        public override bool Equals(object obj)
        {
            var item = obj as Event;

            if (item == null)
            {
                return false;
            }

            return this.Id.Equals(item.Id);
        }
    }
}
