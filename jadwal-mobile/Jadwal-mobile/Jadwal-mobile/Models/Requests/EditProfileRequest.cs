﻿using System;
using System.Collections.Generic;
using System.Text;

namespace jadwal_mobile.Models.Requests
{
    public class EditProfileRequest
    {
        private string _fullName;
        private string _phone;
        private string _password;

        public string FullName { get { return _fullName; } set { _fullName = value; } }
        public string Phone { get { return _phone; } set { _phone = value; } }
        public string Password { get { return _password; } set { _password = value; } }

        public EditProfileRequest(string fullName, string phoneNum, string password)
        {
            _fullName = fullName;
            _phone = phoneNum;  
            _password = password;
        }
    }
}
