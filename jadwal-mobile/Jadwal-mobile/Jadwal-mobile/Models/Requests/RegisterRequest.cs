﻿namespace jadwal_mobile.Models.Requests
{
    public class RegisterRequest
    {
        private string _email;
        private string _password;
        private string _fullName;
        private string _phone;

        public string email { get { return _email; } set { _email = value; } }

        public string password { get { return _password; } set { _password = value; } }

        public string fullName { get { return _fullName; } set { _fullName = value; } }

        public string phone { get { return _phone; } set { _phone = value; } }

        public RegisterRequest(string fullName, string email, string phone, string password)
        {
            _email = email;
            _password = password;
            _fullName = fullName;
            _phone = phone;
        }
    }
}
