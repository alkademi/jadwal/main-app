﻿namespace jadwal_mobile.Models.Requests
{
    public class LoginRequest
    {
        private string _email;
        private string _password;

        public string Email { get { return _email; } set { _email = value; } }
        public string Password { get { return _password; } set { _password = value; } } 

        public LoginRequest(string Email, string Password)
        {
            _email = Email;
            _password = Password;
        }
    }
}
