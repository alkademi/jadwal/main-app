﻿namespace jadwal_mobile.Models.Requests
{
    public class AddEventRequest
    {
        public AddEventRequest()
        {

        }

        public string Title { get; set; }

        public string StartDateTime { get; set; }

        public string EndDateTime { get; set; }

        public string Location { get; set; }

        public string Reminder { get; set; }

        public string Guests { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }
    }
}
