﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace jadwal_mobile.Models
{
    public class JadwalSatuBulan
    {
        public string month_year { get; set; }
        //public Image image { get; set; }
        public ObservableCollection<JadwalSatuHari> JadwalTiapHariList { get; set; }
    }
}
