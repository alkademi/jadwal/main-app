﻿using jadwal_mobile.ViewModels;
using jadwal_mobile.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.CommunityToolkit.Extensions;
using Xamarin.Forms;

namespace jadwal_mobile.Models
{
    public class Jadwal : ContentPage
    {
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string title { get; set; }
        public string color { get; set; }
        public string videoconference { get; set; }
        public int id { get; set; }

        public string information
        {
            get
            {
                return string.Format("{0} - {1} | {2}", start.ToString("HH:mm"), end.ToString("HH:mm"), videoconference);
            }
        }

        public string duration
        {
            get
            {
                return string.Format("{0} - {1}", start.ToString("HH:mm"), end.ToString("HH:mm"));
            }
        }

        //public Command<int> DetailCommand { get { return new Command<int>(DetailPopup); } }

        //private void DetailPopup(int id)
        //{
        //    DetailViewModel detailViewModel = new DetailViewModel
        //    {
        //        ID = id
        //    };
        //    Navigation.ShowPopup(new DetailView(detailViewModel));
        //}
    }
}
