﻿using System.Collections.Generic;
using System.Threading.Tasks;
using jadwal_mobile.Models.Requests;
using jadwal_mobile.Models.Responses;

namespace jadwal_mobile.Services
{
    internal interface IEventService
    {
        Task<Response<FullEventResponse>> GetEventById(int id);
        Task<Response<string>> AddEvent(AddEventRequest request);
        Task<Response<List<EventResponse>>> GetAllEvent();
        Task<Response<string>> UpdateEvent(AddEventRequest request, int id);
        Task<Response<string>> DeleteEvent(int id);
        Task<Response<List<EventResponse>>> GetTodayEvent();
    }
}
