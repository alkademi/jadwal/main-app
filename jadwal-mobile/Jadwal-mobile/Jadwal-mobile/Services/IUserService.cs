﻿using System.Threading.Tasks;

using jadwal_mobile.Models;
using jadwal_mobile.Models.Requests;
using jadwal_mobile.Models.Responses;

namespace jadwal_mobile.Services
{
    public interface IUserService
    {
        Task<Response<string>> Register(RegisterRequest request);
        Task<Response<string>> Login(LoginRequest request);
        Task<Response<string>> Edit(EditProfileRequest request);
        Task<Response<User>> Get();
    }
}
