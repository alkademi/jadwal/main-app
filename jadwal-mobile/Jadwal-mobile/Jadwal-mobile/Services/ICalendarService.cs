﻿using System.Collections.Generic;
using System.Threading.Tasks;
using jadwal_mobile.Models.Responses;

namespace jadwal_mobile.Services
{
    public interface ICalendarService
    {
        Task<Response<List<DateResponse>>> GetMonthly(int year, int month);
    }
}
