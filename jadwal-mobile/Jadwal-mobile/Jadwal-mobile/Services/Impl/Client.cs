﻿using System.Net.Http;

namespace jadwal_mobile.Services.Impl
{
    public static class Client
    {
        private static HttpClient _httpClient;
        public static HttpClient HttpClient
        {
            get
            {
#if DEBUG
                HttpClientHandler insecureHandler = GetInsecureHandler();
                _httpClient = new HttpClient(insecureHandler);
#else
                    _httpClient = new HttpClient();            
#endif
                return _httpClient;
            }
        }

        private static HttpClientHandler GetInsecureHandler()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
            {
                if (cert.Issuer.Equals("CN=localhost"))
                    return true;
                return errors == System.Net.Security.SslPolicyErrors.None;
            };
            return handler;
        }
    }
}
