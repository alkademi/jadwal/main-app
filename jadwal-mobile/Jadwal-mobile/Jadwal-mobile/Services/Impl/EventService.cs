﻿using jadwal_mobile.Models.Requests;
using jadwal_mobile.Models.Responses;
using jadwal_mobile.Utils;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace jadwal_mobile.Services.Impl
{
    internal class EventService : IEventService
    {
        private HttpClient _httpClient;
        private string BASE_PATH;

        public EventService()
        {
            _httpClient = Client.HttpClient;
            BASE_PATH = AppSettings.SERVICE_BASE_PATH_STAGING;
        }

        public async Task<Response<FullEventResponse>> GetEventById(int id)
        {
            string path = BASE_PATH + "event/" + id.ToString();

            ConnectionHelper.SetTokenHeader(_httpClient);

            HttpResponseMessage response = await _httpClient.GetAsync(new Uri(path));
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Response<FullEventResponse>>(responseContent);
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new Exception("Unauthorized!");
            }
            return null;
        }

        public async Task<Response<string>> AddEvent(AddEventRequest request)
        {
            string path = BASE_PATH + "event/add";
            string requestJsonFormat = JsonConvert.SerializeObject(request);
            HttpContent requestContent = new StringContent(requestJsonFormat, Encoding.UTF8, "application/json");

            ConnectionHelper.SetTokenHeader(_httpClient);

            HttpResponseMessage response = await _httpClient.PostAsync(new Uri(path), requestContent);
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Response<string>>(responseContent);
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new Exception("Unauthorized!");
            }
            return null;
        }

        public async Task<Response<List<EventResponse>>> GetAllEvent()
        {
            string path = BASE_PATH + "event";

            ConnectionHelper.SetTokenHeader(_httpClient);

            HttpResponseMessage response = await _httpClient.GetAsync(new Uri(path));
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Response<List<EventResponse>>>(responseContent);
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new Exception("Unauthorized!");
            }
            return null;
        }

        public async Task<Response<string>> UpdateEvent(AddEventRequest request, int id)
        {
            string path = BASE_PATH + "event/update/" + id.ToString();
            string requestJsonFormat = JsonConvert.SerializeObject(request);
            HttpContent requestContent = new StringContent(requestJsonFormat, Encoding.UTF8, "application/json");

            ConnectionHelper.SetTokenHeader(_httpClient);

            HttpResponseMessage response = await _httpClient.PutAsync(new Uri(path), requestContent);
            Console.WriteLine(response);
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Response<string>>(responseContent);
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new Exception("Unauthorized!");
            }
            return null;
        }

        public async Task<Response<string>> DeleteEvent(int id)
        {
            string path = BASE_PATH + "event/delete/" + id.ToString();

            ConnectionHelper.SetTokenHeader(_httpClient);

            HttpResponseMessage response = await _httpClient.DeleteAsync(new Uri(path));
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Response<string>>(responseContent);
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new Exception("Unauthorized!");
            }
            return null;
        }

        public async Task<Response<List<EventResponse>>> GetTodayEvent()
        {
            string path = BASE_PATH + "event/today";

            ConnectionHelper.SetTokenHeader(_httpClient);

            HttpResponseMessage response = await _httpClient.GetAsync(new Uri(path));
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Response<List<EventResponse>>>(responseContent);
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new Exception("Unauthorized!");
            }
            return null;
        }
    }
}
