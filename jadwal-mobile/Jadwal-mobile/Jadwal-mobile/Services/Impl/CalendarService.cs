﻿using jadwal_mobile.Models.Responses;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Essentials;
using jadwal_mobile.Utils;

namespace jadwal_mobile.Services.Impl
{
    internal class CalendarService : ICalendarService
    {
        private HttpClient _httpClient;
        private string BASE_PATH;

        public CalendarService()
        {
            _httpClient = Client.HttpClient;
            BASE_PATH = AppSettings.SERVICE_BASE_PATH_STAGING;
        }

        public async Task<Response<List<DateResponse>>> GetMonthly(int year, int month)
        {
            string path = BASE_PATH + "calendar/" + year.ToString() + "/" + month.ToString();
            try
            {
                ConnectionHelper.SetTokenHeader(_httpClient);
                HttpResponseMessage response = await _httpClient.GetAsync(new Uri(path));
                if (response.IsSuccessStatusCode)
                {
                    string responseContent = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<Response<List<DateResponse>>>(responseContent);
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
                return null;
            }
        }
    }
}
