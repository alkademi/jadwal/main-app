﻿using System;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;

using Newtonsoft.Json;

using jadwal_mobile.Models.Requests;
using jadwal_mobile.Models.Responses;
using jadwal_mobile.Utils;
using System.Net;
using jadwal_mobile.Models;

namespace jadwal_mobile.Services.Impl
{
    internal class UserService : IUserService
    {
        private HttpClient _httpClient;
        private string BASE_PATH;

        public UserService()
        {
            _httpClient = Client.HttpClient;
            BASE_PATH = AppSettings.SERVICE_BASE_PATH_STAGING;
        }

        public async Task<Response<string>> Register(RegisterRequest request)
        {
            string path = BASE_PATH + "user/register";
            try
            {
                string requestJsonFormat = JsonConvert.SerializeObject(request);
                HttpContent requestContent = new StringContent(requestJsonFormat, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await _httpClient.PostAsync(new Uri(path), requestContent);
                if (response.IsSuccessStatusCode)
                {
                    string responseContent = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<Response<string>>(responseContent);
                }
                return null;
            }
            catch(Exception ex)
            {
                Console.Error.WriteLine(ex);
                return null;
            }
        }

        public async Task<Response<string>> Login(LoginRequest request)
        {
            string path = BASE_PATH + "user/login";
            try
            {
                string requestJsonFormat = JsonConvert.SerializeObject(request);
                StringContent requestContent = new StringContent(requestJsonFormat, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await _httpClient.PostAsync(new Uri(path), requestContent);
                Console.WriteLine(response);
                if (response.IsSuccessStatusCode)
                {
                    string responseContent = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<Response<string>>(responseContent);
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
                return null;
            }
        }

        public async Task<Response<string>> Edit(EditProfileRequest request)
        {
            string path = BASE_PATH + "user/edit";
            string requestJsonFormat = JsonConvert.SerializeObject(request);
            HttpContent requestContent = new StringContent(requestJsonFormat, Encoding.UTF8, "application/json");

            ConnectionHelper.SetTokenHeader(_httpClient);

            HttpResponseMessage response = await _httpClient.PutAsync(new Uri(path), requestContent);
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Response<string>>(responseContent);
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new Exception("Unauthorized!");
            }
            return null;
        }

        public async Task<Response<User>> Get()
        {
            string path = BASE_PATH + "user/detail";

            ConnectionHelper.SetTokenHeader(_httpClient);

            HttpResponseMessage response = await _httpClient.GetAsync(new Uri(path));
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Response<User>>(responseContent);
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new Exception("Unauthorized!");
            }
            return null;
        }
    }
}
