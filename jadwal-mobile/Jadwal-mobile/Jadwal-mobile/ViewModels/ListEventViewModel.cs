﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Globalization;

using jadwal_mobile.Services;
using jadwal_mobile.Services.Impl;
using jadwal_mobile.Models;
using jadwal_mobile.Models.Responses;
using jadwal_mobile.Utils;
using Xamarin.Forms;
using jadwal_mobile.Views;
using Xamarin.CommunityToolkit.Extensions;

namespace jadwal_mobile.ViewModels
{
    public class ListEventViewModel: INotifyPropertyChanged
    {
        private IEventService _eventService;

        public ListEventViewModel()
        {
            _eventService = new EventService();

            FetchEvent(); 
        }

        
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private ObservableCollection<EventsMonth> eventsMonths;
        public ObservableCollection<EventsMonth> EventsMonths 
        {
            get { return eventsMonths; }
            set 
            { 
                eventsMonths = value;
                OnPropertyChanged(nameof(EventsMonths));
            } 
        }

        public async void FetchEvent()
        {
            EventsMonths = new ObservableCollection<EventsMonth>();
            try
            {
                Response<List<EventResponse>> response = await _eventService.GetAllEvent();
                if (response.Code == 200)
                {
                    List<EventResponse> events = response.Data;
                    string prevMonthYear = "Temporary Value";
                    int prevDay = 9999;
                    foreach (EventResponse eventResponse in events)
                    {
                        Console.WriteLine(eventResponse.StartDateTime);
                        Event _event = EventHelper.ToEvent(eventResponse);
                        string monthYear = String.Format("{0:MMMM yyyy}", _event.StartDateTime);
                        
                        int day = _event.StartDateTime.Day;

                        if (monthYear == prevMonthYear)
                        {
                            int lastIdxMonth = EventsMonths.Count - 1;
                            if (day == prevDay)
                            {
                                int lastIdxDay = EventsMonths[lastIdxMonth].EventsDays.Count - 1;
                                EventsMonths[lastIdxMonth].EventsDays[lastIdxDay].Events.Add(_event);
                            }
                            else
                            {
                                EventsDay eventsDay = new EventsDay();
                                eventsDay.Day = day;

                                eventsDay.Events.Add(_event);
                                EventsMonths[lastIdxMonth].EventsDays.Add(eventsDay);
                            }
                        }
                        else
                        {
                            EventsMonth eventsMonth = new EventsMonth();
                            eventsMonth.MonthYear = monthYear;
                            
                            EventsDay eventsDay = new EventsDay();
                            eventsDay.Day = day;

                            eventsDay.Events.Add(_event);
                            eventsMonth.EventsDays.Add(eventsDay);
                            EventsMonths.Add(eventsMonth);
                        }

                        prevDay = day;
                        prevMonthYear = monthYear;
                    }
                }
                else
                {
                    Console.WriteLine("Error!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    public class EventsMonth
    {
        public string MonthYear { get; set; }
        public ObservableCollection<EventsDay> EventsDays { get; set; }

        public EventsMonth()
        {
            EventsDays = new ObservableCollection<EventsDay>();
        }
    }

    public class EventsDay
    {
        public int Day { get; set; }
        public ObservableCollection<Event> Events { get; set; }

        public EventsDay()
        {
            Events = new ObservableCollection<Event>();
        }
    }
}
