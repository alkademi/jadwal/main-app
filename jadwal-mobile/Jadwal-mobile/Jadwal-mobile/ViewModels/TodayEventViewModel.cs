﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

using jadwal_mobile.Services;
using jadwal_mobile.Services.Impl;
using jadwal_mobile.Models;
using jadwal_mobile.Models.Responses;
using jadwal_mobile.Utils;

using System.Collections.Generic;
using System;
using System.Collections.ObjectModel;

namespace jadwal_mobile.ViewModels
{
    public class TodayEventViewModel : INotifyPropertyChanged
    {
        private IEventService _eventService;

        public TodayEventViewModel()
        {
            _eventService = new EventService();
            TodayEvents = new ObservableCollection<Event>();
            countOfEvents = 0;
            FetchToday();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ObservableCollection<Event> TodayEvents { get; set; }

        private int countOfEvents;
        public int CountOfEvents //diubah biar dia ngitungnya satu per satu
        { 
            get { return countOfEvents; }
            set
            {
                countOfEvents = value;
                OnPropertyChanged(nameof(CountOfEvents));
            }
        }

        private string todayText;
        public string TodayText 
        { 
            get { return todayText; } 
            set
            {
                todayText = value;
                OnPropertyChanged(nameof(TodayText));
            }
        }

        public async void FetchToday()
        {
            try
            {
                Response<List<EventResponse>> response = await _eventService.GetTodayEvent();
                if (response.Code == 200)
                {
                    foreach(EventResponse eventResponse in response.Data)
                    {
                        if (!TodayEvents.Contains(EventHelper.ToEvent(eventResponse)))
                        {
                            TodayEvents.Add(EventHelper.ToEvent(eventResponse));
                            CountOfEvents++;
                        }
                    }

                    if (TodayEvents.Count > 0)
                    {
                        TodayText = TodayEvents[0].StartDateTime.ToString("dddd dd MMMM yyyy");
                    }
                    else
                    {
                        TodayText = DateTime.Now.ToString("dddd dd MMMM yyyy");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
