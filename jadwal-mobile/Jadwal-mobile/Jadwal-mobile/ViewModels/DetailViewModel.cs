﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using jadwal_mobile.Models;
using jadwal_mobile.Models.Requests;
using jadwal_mobile.Models.Responses;
using jadwal_mobile.Services;
using jadwal_mobile.Services.Impl;
using jadwal_mobile.Utils;
using jadwal_mobile.Views;
using Xamarin.Forms;

namespace jadwal_mobile.ViewModels
{
    public class DetailViewModel: INotifyPropertyChanged
    {
        private readonly IEventService _eventService;
        public DetailViewModel(int id)
        {
            _eventService = new EventService();
            guests = "";
            remindersList = new ObservableCollection<Reminder>()
            {
                new Reminder(){Key = 1, ValueText="5 menit", Value=new TimeSpan(0,5,0)},
                new Reminder(){Key = 2, ValueText="10 menit", Value=new TimeSpan(0,10,0)},
                new Reminder(){Key = 3, ValueText="30 menit", Value=new TimeSpan(0,30,0)},
                new Reminder(){Key = 4, ValueText="1 jam", Value=new TimeSpan(1,0,0)},
                new Reminder(){Key = 5, ValueText="3 jam", Value=new TimeSpan(3,0,0)},
                new Reminder(){Key = 6, ValueText="6 jam", Value=new TimeSpan(6,0,0)},
            };
            Task.Run(() =>
            {
                FetchEvent(id);
            }).Wait();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private int id;
        public int Id
        {
            get { return id; }
            set
            {
                id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        private ObservableCollection<Reminder> remindersList;
        public ObservableCollection<Reminder> RemindersList
        {
            get { return remindersList; }
            set
            {
                remindersList = value;
                OnPropertyChanged(nameof(remindersList));
            }
        }

        private string titleEvent { get; set; }
        public string TitleEvent
        {
            get { return titleEvent; }
            set
            {
                titleEvent = value;
                OnPropertyChanged(nameof(TitleEvent));
            }
        }
        private DateTime startDateTime { get; set; }
        public DateTime StartDateTime
        {
            get { return startDateTime; }
            set
            {
                startDateTime = value;
                OnPropertyChanged(nameof(StartDateTime));
                OnPropertyChanged(nameof(Information));
            }
        }
        
        private DateTime endDateTime { get; set; }
        public DateTime EndDateTime
        {
            get { return endDateTime; }
            set
            {
                endDateTime = value;
                OnPropertyChanged(nameof(EndDateTime));
                OnPropertyChanged(nameof(Information));
            }
        }
        
        private string location { get; set; }
        public string Location
        {
            get { return location; }
            set
            {
                location = value;
                OnPropertyChanged(nameof(Location));
            }
        }
        private Reminder selectedReminder { get; set; }
        public Reminder SelectedReminder
        {
            get { return selectedReminder; }
            set
            {
                selectedReminder = value;
                OnPropertyChanged(nameof(SelectedReminder));
            }
        }
        private String guests { get; set; }
        public String Guests
        {
            get { return guests; }
            set
            {
                guests = value;
                OnPropertyChanged(nameof(Guests));
                OnPropertyChanged(nameof(GuestList));
                OnPropertyChanged(nameof(CountGuest));
            }
        }
        
        private Color selectedColor { get; set; }
        public Color SelectedColor
        {
            get { return selectedColor; }
            set
            {
                if (selectedColor != value)
                {
                    selectedColor = value;
                    OnPropertyChanged(nameof(SelectedColor));
                }
            }
        }

        private string description { get; set; }
        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public string Information 
        { 
            get
            {
                return StartDateTime.ToString("dd MMMM yy") + " | " + string.Format("{0} - {1}", StartDateTime.ToString("HH:mm"), EndDateTime.ToString("HH:mm"));
            }
        }

        public ObservableCollection<Guest> GuestList
        {
            get
            {
                var list = new ObservableCollection<Guest>();
                var listguest = guests.Split(',');
                foreach (var guest in listguest)
                {
                    var _guest = new Guest() { Content = guest.Trim()};
                    list.Add(_guest);
                }
                return list;
            }
        }
        public int CountGuest
        {
            get
            {
                return GuestList.Count;
            }
        }

        private async void FetchEvent(int id)
        {
            try
            {
                Response<FullEventResponse> response = await _eventService.GetEventById(id);
                if (response.Code == 200)
                {
                    Id = id;
                    FullEvent fullEvent = EventHelper.ToFullEvent(response.Data);
                    TitleEvent = fullEvent.Title;
                    Location = fullEvent.Location;
                    StartDateTime = fullEvent.StartDateTime;
                    EndDateTime = fullEvent.EndDateTime;
                    SelectedColor = new Color() { Hex = fullEvent.Color };
                    foreach (Reminder reminder in remindersList) {
                        if (reminder.Value.Equals(fullEvent.Reminder)) { 
                            SelectedReminder = reminder;
                            break;
                        }
                    }
                    Guests = fullEvent.Guests;
                    Description = fullEvent.Description;
                }
                else
                {
                    Console.WriteLine("Error!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Command UpdateEventCommand { get { return new Command(SubmitUpdateEvent); } }
        private async void SubmitUpdateEvent()
        {
            if (titleEvent != null && startDateTime != null && endDateTime != null && selectedReminder != null && selectedColor != null)
            {
                try
                {
                    //tembak event service
                    AddEventRequest request = new AddEventRequest()
                    {
                        Title = titleEvent,
                        StartDateTime = startDateTime.ToString(),
                        EndDateTime = endDateTime.ToString(),
                        Location = location,
                        Reminder = selectedReminder.Value.ToString(),
                        Guests = guests,
                        Color = selectedColor.Hex,
                        Description = description
                    };

                    Response<string> response = await _eventService.UpdateEvent(request, Id);
                    if (response != null && response.Code == 200)
                    {
                        Console.WriteLine(response.Message);
                        await App.Current.MainPage.DisplayAlert("Success", "Jadwal berhasil diupdate", "Ok");
                        //titleEvent = null;
                        //startDate = DateTime.Now;
                        //endDate = DateTime.Now;
                        //startTime = TimeSpan.Zero;
                        //endTime = TimeSpan.Zero;
                        //selectedReminder = null;
                        //selectedColor = null;
                    }
                }
                catch (Exception e)
                {
                    await App.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");
                    await App.Current.MainPage.Navigation.PushAsync(new LoginView());
                }
            }
            else
            {
                Console.WriteLine("Masuk sini bos");
            }
        }

        public Command DeleteEventCommand { get { return new Command(SubmitDeleteEvent); } }
        private async void SubmitDeleteEvent()
        {
            try
            {
                //tembak event service
                Response<string> response = await _eventService.DeleteEvent(Id);
                if (response != null && response.Code == 200)
                {
                    Console.WriteLine(response.Message);
                    await App.Current.MainPage.DisplayAlert("Success", "Jadwal berhasil dihapus", "Ok");
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");
                await App.Current.MainPage.Navigation.PushAsync(new LoginView());
            }
        }

        public class Guest
        {
            public string Content { get; set; }
        }
    }
}
