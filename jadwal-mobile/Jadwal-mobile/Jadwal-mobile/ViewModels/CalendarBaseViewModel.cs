﻿using jadwal_mobile.Models;
using jadwal_mobile.Models.Responses;
using jadwal_mobile.Services;
using jadwal_mobile.Services.Impl;
using jadwal_mobile.Views;
using Syncfusion.SfSchedule.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.CommunityToolkit.Extensions;
using Xamarin.Forms;

namespace jadwal_mobile.ViewModels
{
    public class CalendarBaseViewModel : ContentPage
    {
        private ICalendarService _calendarService;
        public ObservableCollection<Meeting> Meetings { get; set; }
        public static int month { get; set; }
        public static int year { get; set; }
        public ICommand ScheduleCellTapped { get; set; }
        public ICommand ScheduleVisibleDatesChanged { get; set; }

        public CalendarBaseViewModel()
        {
            Meetings = new ObservableCollection<Meeting>();
            _calendarService = new CalendarService();
            ScheduleVisibleDatesChanged = new Command<VisibleDatesChangedEventArgs>(VisibleDatesChanged);
        }

        public void VisibleDatesChanged(VisibleDatesChangedEventArgs args)
        {
            List<DateTime> dateList = new List<DateTime>();
            dateList = (args.visibleDates);
            var item = dateList[dateList.Count / 2];
            var monthDate = item.Date.Month;
            var yearDate = item.Date.Year;
            //Console.WriteLine(item.Date.ToString("MMMM, YY"));
            FetchDataMonthly(monthDate, yearDate);
        }

        public async void FetchDataMonthly(int _month, int _year)
        {
            if (_month != month || _year != year)
            {
                month = _month;
                year = _year;
                Meetings.Clear();
                Response<List<DateResponse>> response = await _calendarService.GetMonthly(_year, _month);
                if (response.Code == 200)
                {
                    List<DateResponse> days = response.Data;
                    for (int numOfDays = 0; numOfDays < days.Count; numOfDays++)
                    {
                        for (int numOfEvents = 0; numOfEvents < days[numOfDays].Events.Count; numOfEvents++)
                        {
                            Meeting events = new Meeting();

                            string[] startDateTimeString = days[numOfDays].Events[numOfEvents].StartDateTime.Split(' ');
                            string[] startDateString = startDateTimeString[0].Split('/');
                            string[] startTimeString = startDateTimeString[1].Split(':');
                            string[] endDateTimeString = days[numOfDays].Events[numOfEvents].EndDateTime.Split(' ');
                            string[] endDateString = endDateTimeString[0].Split('/');
                            string[] endTimeString = endDateTimeString[1].Split(':');

                            events.Id = days[numOfDays].Events[numOfEvents].Id;
                            events.From = new DateTime(Int32.Parse(startDateString[2]), Int32.Parse(startDateString[0]), Int32.Parse(startDateString[1]),
                                Int32.Parse(startTimeString[0]), Int32.Parse(startTimeString[1]), Int32.Parse(startTimeString[2]));
                            events.To = new DateTime(Int32.Parse(endDateString[2]), Int32.Parse(endDateString[0]), Int32.Parse(endDateString[1]),
                                Int32.Parse(endTimeString[0]), Int32.Parse(endTimeString[1]), Int32.Parse(endTimeString[2]));
                            events.EventName = days[numOfDays].Events[numOfEvents].Title;
                            events.IsHoliday = false;
                            events.color = Xamarin.Forms.Color.FromHex(days[numOfDays].Events[numOfEvents].Color);
                            Meetings.Add(events);
                        }

                        if (days[numOfDays].Holiday.IsHoliday)
                        {
                            //Console.WriteLine(days[numOfDays].Holiday.Title.ToString());
                            Meeting events = new Meeting();
                            events.From = new DateTime(_year, _month, days[numOfDays].Date, 0, 0, 1);
                            events.To = new DateTime(_year, _month, days[numOfDays].Date, 23, 59, 59);
                            events.EventName = days[numOfDays].Holiday.Title.ToString();
                            events.AllDay = true;
                            events.IsHoliday = true;
                            events.color = Xamarin.Forms.Color.FromHex("#FF0000");
                            Meetings.Add(events);
                        }
                    }
                }
            }
        }
    }
}
