﻿using jadwal_mobile.Services;
using jadwal_mobile.Services.Impl;
using jadwal_mobile.Models.Requests;
using jadwal_mobile.Models.Responses;
using jadwal_mobile.Views;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace jadwal_mobile.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private IUserService _userService;

        public LoginViewModel()
        {
            _userService = new UserService();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private string email;
        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                OnPropertyChanged(nameof(email));
            }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                OnPropertyChanged(nameof(password));
            }
        }

        public Command SubmitCommand { get { return new Command(Submit); } }
        private async void Submit()
        {
            if (email != null && password != null)
            {
                Response<string> response = await _userService.Login(new LoginRequest(email, password));
                if (response != null && response.Code == 200)
                {
                    // Simpan Token
                    await SecureStorage.SetAsync("token", response.Data);
                    await App.Current.MainPage.Navigation.PushAsync(new DashboardView());
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error", response.Message, "Ok");
                }
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("Error", "Benahi masukan Anda!", "Ok");
            }
        }

    }
}