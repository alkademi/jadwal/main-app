﻿using jadwal_mobile.Services;
using jadwal_mobile.Services.Impl;
using jadwal_mobile.Models;
using jadwal_mobile.Models.Requests;
using jadwal_mobile.Models.Responses;
using jadwal_mobile.Views;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using System;
using Xamarin.Forms;

namespace jadwal_mobile.ViewModels
{
    public class ProfileViewModel: INotifyPropertyChanged
    {
        private IUserService _userService;

        public ProfileViewModel()
        {
            _userService = new UserService();
            FetchUser();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /* Edit Profile */
        public async void FetchUser()
        {
            try
            {
                Response<User> user = await _userService.Get();
                if (user.Code == 200)
                {
                    FullName = user.Data.FullName;
                    Email = user.Data.Email;
                    PhoneNum = user.Data.Phone;
                    Password = user.Data.Password;
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");
                await App.Current.MainPage.Navigation.PushAsync(new LoginView());
            }
        }

        private string fullName { get; set; }
        public string FullName
        {
            get { return fullName; }
            set
            {
                fullName = value;
                OnPropertyChanged(nameof(FullName));
            }
        }
        private string email { get; set; }
        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                OnPropertyChanged(nameof(Email));
            }
        }
        private string phoneNum { get; set; }
        public string PhoneNum
        {
            get { return phoneNum; }
            set
            {
                phoneNum = value;
                OnPropertyChanged(nameof(PhoneNum));
            }
        }
        private string password { get; set; }
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                OnPropertyChanged(nameof(Password));
            }
        }
        public Command SubmitEditProfileCommand { get { return new Command(SubmitEditProfile); } }
        private async void SubmitEditProfile()
        {
            if (email != null && password != null && fullName != null && phoneNum != null)
            {
                Response<string> response = await _userService.Edit(new EditProfileRequest(fullName, phoneNum, password));
                if (response != null && response.Code == 200)
                {
                    await App.Current.MainPage.DisplayAlert("Sucess", response.Message, "Ok");
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error", response.Message, "Ok");
                }
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("Error", "Benahi masukan Anda!", "Ok");
            }
        }
    }
}
