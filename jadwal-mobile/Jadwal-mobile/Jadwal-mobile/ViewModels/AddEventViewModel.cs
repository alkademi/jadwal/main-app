﻿using jadwal_mobile.Services;
using jadwal_mobile.Services.Impl;
using jadwal_mobile.Models.Requests;
using jadwal_mobile.Models.Responses;
using jadwal_mobile.Views;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace jadwal_mobile.ViewModels
{
    public class AddEventViewModel: INotifyPropertyChanged
    {
        private IEventService _eventService;

        public AddEventViewModel()
        {
            _eventService = new EventService();

            startDate = DateTime.Today;
            endDate = DateTime.Today;

            DateTime start = DateTime.Now;
            DateTime end = start.AddMinutes(30);
            TimeSpan _startTime = start.TimeOfDay;
            TimeSpan _endTime = end.TimeOfDay;
            startTime = _startTime;
            endTime = _endTime;

            remindersList = new ObservableCollection<Reminder>()
            {
                new Reminder(){Key = 1, ValueText="5 menit", Value=new TimeSpan(0,5,0)},
                new Reminder(){Key = 2, ValueText="10 menit", Value=new TimeSpan(0,10,0)},
                new Reminder(){Key = 3, ValueText="30 menit", Value=new TimeSpan(0,30,0)},
                new Reminder(){Key = 4, ValueText="1 jam", Value=new TimeSpan(1,0,0)},
                new Reminder(){Key = 5, ValueText="3 jam", Value=new TimeSpan(3,0,0)},
                new Reminder(){Key = 6, ValueText="6 jam", Value=new TimeSpan(6,0,0)},
            };

            colorsList = new ObservableCollection<Color>()
            {
                new Color(){Key = 1, ColorValue="Red", Hex="#FFC09F"},
                new Color(){Key = 2, ColorValue="Blue", Hex="#A0CED9"},
                new Color(){Key = 3, ColorValue="Green", Hex="#ADF7B6"},
                new Color(){Key = 4, ColorValue="Yellow", Hex="#FFEE93"},
                new Color(){Key = 5, ColorValue="Grey", Hex="#F6EDEE"},
                new Color(){Key = 6, ColorValue="Orange", Hex="#FCF5C7"}
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private ObservableCollection<Reminder> remindersList;
        public ObservableCollection<Reminder> RemindersList
        {
            get { return remindersList; }
            set
            {
                remindersList = value;
                OnPropertyChanged(nameof(RemindersList));
            }
        }

        private ObservableCollection<Color> colorsList;
        public ObservableCollection<Color> ColorsList
        {
            get { return colorsList; }
            set
            {
                colorsList = value;
                OnPropertyChanged(nameof(ColorsList));
            }
        }

        private String titleEvent { get; set; }
        public String TitleEvent
        {
            get { return titleEvent; }
            set
            {
                titleEvent = value;
                OnPropertyChanged(nameof(TitleEvent));
            }
        }
        private DateTime startDate { get; set; }
        public DateTime StartDate
        {
            get { return startDate; }
            set
            {
                startDate = value;
                OnPropertyChanged(nameof(StartDate));
                Validate();
            }
        }
        private TimeSpan startTime { get; set; }
        public TimeSpan StartTime
        {
            get { return startTime; }
            set
            {
                startTime = value;
                OnPropertyChanged(nameof(StartTime));
                Validate();
            }
        }
        private DateTime endDate { get; set; }
        public DateTime EndDate
        {
            get { return endDate; }
            set
            {
                endDate = value;
                OnPropertyChanged(nameof(EndDate));
                Validate();
            }
        }
        private TimeSpan endTime { get; set; }
        public TimeSpan EndTime
        {
            get { return endTime; }
            set
            {
                endTime = value;
                OnPropertyChanged(nameof(EndTime));
                Validate();
            }
        }
        private string location { get; set; }
        public string Location
        {
            get { return location; }
            set
            {
                location = value;
                OnPropertyChanged(nameof(Location));
            }
        }
        private Reminder selectedReminder { get; set; }
        public Reminder SelectedReminder
        {
            get { return selectedReminder; }
            set
            {
                selectedReminder = value;
                OnPropertyChanged(nameof(SelectedReminder));
            }
        }
        private String guests { get; set; }
        public String Guests
        {
            get { return guests; }
            set
            {
                guests = value;
                OnPropertyChanged(nameof(Guests));
            }
        }
        private Color selectedColor { get; set; }
        public Color SelectedColor
        {
            get { return selectedColor; }
            set
            {
                if (selectedColor != value)
                {
                    selectedColor = value;
                    OnPropertyChanged(nameof(SelectedColor));
                    Colored = Xamarin.Forms.Color.FromHex(selectedColor.Hex);
                }
            }
        }

        private Xamarin.Forms.Color colored;
        public Xamarin.Forms.Color Colored
        {
            get { return colored; }
            set
            {
                colored = value;
                OnPropertyChanged(nameof(Colored));
            }
        }

        private String description { get; set; }
        public String Description
        {
            get { return description; }
            set
            {
                description = value;
                OnPropertyChanged(nameof(Description));
            }
        }
        private bool isShowError = false;
        public bool Validate()
        {
            TimeSpan startTimeSpan = TimeSpan.FromTicks(startDate.Ticks) + startTime;
            TimeSpan endTimeSpan = TimeSpan.FromTicks(endDate.Ticks) + endTime;
            if (startTimeSpan >= endTimeSpan)
            {
                isShowError = true;
                OnPropertyChanged(nameof(IsShowError));
                return false;
            } else
            {
                isShowError = false;
                OnPropertyChanged(nameof(IsShowError));
                return true;
            }
        }
        public bool IsShowError
        {
            get { return isShowError; }
            set
            {
                isShowError = value;
                OnPropertyChanged();
            }
        }
        public Command SubmitAddEventCommand { get { return new Command(SubmitAddEvent); } }
        private async void SubmitAddEvent()
        {
            Console.WriteLine(Validate());
            if (titleEvent != null && startDate != null && startTime != null && endDate != null && endTime != null &&
                selectedReminder != null && selectedColor != null && Validate())
            {
                try
                {
                    //tembak event service
                    //Console.WriteLine(startDate.ToString());
                    //Console.WriteLine(startTime.ToString());
                    //Console.WriteLine(startDate.Add(startTime).ToString());
                    AddEventRequest request = new AddEventRequest()
                    {
                        Title = titleEvent,
                        StartDateTime = startDate.Add(startTime).ToString(),
                        EndDateTime = endDate.Add(endTime).ToString(),
                        Location = location,
                        Reminder = selectedReminder.Value.ToString(),
                        Guests = guests,
                        Color = selectedColor.Hex,
                        Description = description
                    };

                    Response<string> response = await _eventService.AddEvent(request);
                    if (response != null && response.Code == 200)
                    {
                        //Console.WriteLine(response.Message);
                        await App.Current.MainPage.DisplayAlert("Success", "Jadwal berhasil ditambahkan", "Ok");
                        TitleEvent = null;
                        StartDate = DateTime.Today;
                        EndDate = DateTime.Today;
                        StartTime = DateTime.Now.TimeOfDay;
                        EndTime = DateTime.Now.AddMinutes(30).TimeOfDay;
                        Location = "";
                        Description = "";
                        Guests = "";
                    }
                    else
                    {
                        await App.Current.MainPage.DisplayAlert("Error", response.Message, "Ok");
                    }
                }
                catch (Exception e)
                {
                    await App.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");
                    await App.Current.MainPage.Navigation.PushAsync(new LoginView());
                }
            }
            else
            {
                Console.WriteLine("Masuk sini boi");
            }
        }
    }

    public class Reminder
    {
        public int Key { get; set; }
        public string ValueText { get; set; }
        public TimeSpan Value { get; set; }
    }

    public class Color
    {
        public int Key { get; set; }
        public String ColorValue { get; set; }
        public String Hex { get; set; }
    }
}
