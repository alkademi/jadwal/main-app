﻿using System.ComponentModel;
using Xamarin.Forms;

using jadwal_mobile.Services;
using jadwal_mobile.Services.Impl;
using jadwal_mobile.Models.Requests;
using jadwal_mobile.Models.Responses;
using jadwal_mobile.Views;
using Xamarin.Essentials;

namespace jadwal_mobile.ViewModels
{
    public class RegisterViewModel: INotifyPropertyChanged
    {
        private IUserService _userService;
        public RegisterViewModel()
        {
            _userService = new UserService();
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private string fullName;
        public string FullName
        {
            get { return fullName; }
            set 
            { 
                fullName = value; 
                PropertyChanged(this, new PropertyChangedEventArgs(FullName));
            }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                PropertyChanged(this,new PropertyChangedEventArgs(Email));
            }
        }

        private string phoneNum;
        public string PhoneNum
        {
            get { return phoneNum; }
            set
            {
                phoneNum = value;
                PropertyChanged(this, new PropertyChangedEventArgs(PhoneNum));
            }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                PropertyChanged(this, new PropertyChangedEventArgs(Password));
            }
        }

        public Command SubmitCommand { get { return new Command(Submit); } }
        private async void Submit()
        {
            if (fullName!=null && email!=null && phoneNum!=null && password != null)
            {
                Response<string> response = await _userService.Register(new RegisterRequest(fullName, email, phoneNum, password));
                if (response != null && response.Code==200)
                {
                    // Simpan Token
                    await SecureStorage.SetAsync("token", response.Data);
                    await App.Current.MainPage.Navigation.PushAsync(new DashboardView());
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error", response.Message, "Ok");
                }
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("Error", "Benahi masukan Anda!", "Ok");
            }
        }
    }
}
