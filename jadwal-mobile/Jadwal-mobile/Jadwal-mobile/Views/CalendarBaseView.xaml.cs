﻿using System;

using jadwal_mobile.ViewModels;
using jadwal_mobile.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.CommunityToolkit.Extensions;

namespace jadwal_mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalendarBaseView : ContentView
    {
        public CalendarBaseView()
        {
            InitializeComponent();
        }

        private void schedule_weekly_CellTapped(object sender, Syncfusion.SfSchedule.XForms.CellTappedEventArgs e)
        {
            var selectedAppointment = e.Appointment as Meeting;
            if (selectedAppointment != null && !(selectedAppointment.IsHoliday))
            {
                Console.WriteLine("Id : " + selectedAppointment.Id.ToString());
                Console.WriteLine("EventName : " + selectedAppointment.EventName.ToString());
                Console.WriteLine("From : " + selectedAppointment.From.ToString());
                Console.WriteLine("To : " + selectedAppointment.To.ToString());

                Navigation.ShowPopup(new DetailView(selectedAppointment.Id));
            }
        }
    }
}