﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using jadwal_mobile.ViewModels;

namespace jadwal_mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfileView : ContentPage
    {
        public ProfileViewModel ProfileViewModel;
        private bool isEditable = false;

        public ProfileView()
        {
            InitializeComponent();
            ProfileViewModel = new ProfileViewModel();
            this.BindingContext = ProfileViewModel;
            SetEditable(isEditable);
        }

        private async void ImageButton_Clicked(object sender, EventArgs e)
        {
            await imgButtonEditProfile.ScaleTo(0.75, 100);
            await imgButtonEditProfile.ScaleTo(1, 100);

            isEditable = !isEditable;
            SetEditable(isEditable);
        }

        private void SetEditable(bool val)
        {
            entryName.IsEnabled = val;
            entryPhoneNum.IsEnabled = val;
            entryPassword.IsEnabled = val;
            submitEditProfileButton.IsEnabled = val;
        }

        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            Console.WriteLine("Test 2");
        }
    }
}