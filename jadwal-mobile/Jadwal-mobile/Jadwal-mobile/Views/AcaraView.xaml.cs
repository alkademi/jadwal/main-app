﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using jadwal_mobile.Models;
using jadwal_mobile.ViewModels;
using Xamarin.CommunityToolkit.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace jadwal_mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AcaraView : ContentPage
    {
        public ListEventViewModel ListEventViewModel;
        public AcaraView()
        {
            InitializeComponent();
            ListEventViewModel = new ListEventViewModel();
            this.BindingContext = ListEventViewModel;
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            var te = (TappedEventArgs)e;
            int param = (int)te.Parameter;
            Navigation.ShowPopup(new DetailView(param));
        }
    }
}