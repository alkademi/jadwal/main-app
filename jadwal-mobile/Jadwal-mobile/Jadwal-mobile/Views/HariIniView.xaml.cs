﻿using System;
using Xamarin.CommunityToolkit.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using jadwal_mobile.ViewModels;

namespace jadwal_mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HariIniView : ContentPage
    {
        public TodayEventViewModel TodayEventViewModel { get; set;}
        public HariIniView()
        {
            InitializeComponent();
            TodayEventViewModel = new TodayEventViewModel();
            this.BindingContext = TodayEventViewModel;
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            var te = (TappedEventArgs)e;
            int param = (int)te.Parameter;
            Navigation.ShowPopup(new DetailView(param));
        }
    }
}