﻿using Xamarin.CommunityToolkit.UI.Views;
using jadwal_mobile.ViewModels;
using jadwal_mobile.Services;
using jadwal_mobile.Services.Impl;
using System;
using jadwal_mobile.Models.Responses;

namespace jadwal_mobile.Views
{
    public partial class DetailView : Popup
    {
        private DetailViewModel _viewModel;
        private bool isEditable = false;
        private IEventService _eventService;

        public DetailView(int id)
        {
            InitializeComponent();
            _viewModel = new DetailViewModel(id);
            this.BindingContext = _viewModel;
            _eventService = new EventService();
            SetEditable(isEditable);
        }

        private void SetEditable(bool val)
        {

        }

        private void edit_button_Clicked(object sender, System.EventArgs e)
        {
            Console.WriteLine("Edit Button Clicked");
            _viewModel.UpdateEventCommand.Execute(null);
        }

        private void close_button_Clicked(object sender, System.EventArgs e)
        {
            Console.WriteLine("Close Button Clicked");
        }

        private async void delete_button_Clicked(object sender, System.EventArgs e)
        {
            Console.WriteLine("Delete Button Clicked");
            _viewModel.DeleteEventCommand.Execute(null);
        }
    }
}