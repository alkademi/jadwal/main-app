﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using jadwal_mobile.Utils;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace jadwal_mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginView : ContentPage
    {

        public LoginView()
        {
            InitializeComponent();

            Register_Tapped();
        }

        private void Login_Clicked(object sender, EventArgs e)
        {
        }

        void Register_Tapped()
        {
            SignUpLabel.GestureRecognizers.Add(new TapGestureRecognizer()
            {
                Command = new Command(() =>
                {
                    var registerView = new RegisterView();

                    NavigationPage.SetHasNavigationBar(registerView, false);

                    Application.Current.MainPage.Navigation.PushAsync(registerView);
                })
            });
        }

        //public bool IsEmail(string Email)
        //{
        //    Email = Email == null ? "" : Email;
        //    string RegexMatcher = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
        //    Regex Re = new Regex(RegexMatcher, RegexOptions.IgnoreCase);

        //    if (Re.IsMatch(Email))
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        //public void DoLogin(string Email, string Password)
        //{
        //    LoginRequest request = new LoginRequest(Email, Password);
        //    LoginResponse response = _userService.Login(request).Result;

        //    if (response != null)
        //    {
        //        Console.WriteLine("Berhasil");
        //    }
        //    Console.WriteLine("Gagal");
        //}
    }
}