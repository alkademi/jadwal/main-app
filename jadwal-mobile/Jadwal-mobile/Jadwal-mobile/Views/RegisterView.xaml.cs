﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using jadwal_mobile.Utils;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace jadwal_mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterView : ContentPage
    {

        public RegisterView()
        {
            InitializeComponent();
            Login_Tapped();
        }

        private void Login_Tapped()
        {
            LoginLabel.GestureRecognizers.Add(new TapGestureRecognizer()
            {
                Command = new Command(() =>
                {
                    var loginView = new LoginView();

                    App.Current.MainPage = new NavigationPage(loginView);

                    NavigationPage.SetHasNavigationBar(loginView, false);
                })
            });
        }

        //private async void Register_Clicked(object sender, EventArgs e)
        //{
        //    bool IsEmail = _registerViewModel.IsEmail(EntryEmail.Text);
        //    bool EmailUnused = _registerViewModel.ValidateEmailUnused(EntryEmail.Text);
        //    bool IsPhone = _registerViewModel.IsPhone(EntryPhone.Text);
        //    bool PhoneUnused = _registerViewModel.ValidatePhoneUnused(EntryPhone.Text);
        //    bool PasswordMatch = _registerViewModel.ValidatePassword(EntryPassword.Text, EntryConfirm.Text);

        //    if (IsEmail && EmailUnused && IsPhone && PhoneUnused && PasswordMatch)
        //    {
        //        _registerViewModel.DoRegister(EntryFullName.Text, EntryEmail.Text, EntryPhone.Text, EntryPassword.Text);
        //    }
        //    else
        //    {
        //        await DisplayAlert("Error", "Benahi Masukan Anda!", "OK");
        //    }
        //    await Navigation.PushAsync(_dashboardView);
        //}

        //private void EntryEmail_Unfocused(object sender, FocusEventArgs e)
        //{
        //    if (!_registerViewModel.IsEmail(EntryEmail.Text))
        //    {
        //        EntryEmail.TextColor = Color.Red;
        //    }

        //    if (!_registerViewModel.ValidateEmailUnused(EntryEmail.Text))
        //    {
        //        EntryEmail.TextColor = Color.Red;
        //    }

        //}

        //private void EntryEmail_Focused(object sender, FocusEventArgs e)
        //{
        //    EntryEmail.TextColor = Color.Black;
        //}

        //private void EntryPhone_Unfocused(object sender, FocusEventArgs e)
        //{
        //    if (!_registerViewModel.IsPhone(EntryPhone.Text))
        //    {
        //        EntryPhone.TextColor = Color.Red;
        //    }

        //    if (!_registerViewModel.ValidatePhoneUnused(EntryPhone.Text))
        //    {
        //        EntryPhone.TextColor = Color.Red;
        //    }

        //}

        //private void EntryPhone_Focused(object sender, FocusEventArgs e)
        //{
        //    EntryPhone.TextColor = Color.Black;
        //}

        //private void EntryConfirm_Unfocused(object sender, FocusEventArgs e)
        //{
        //    if (!_registerViewModel.ValidatePassword(EntryPassword.Text, EntryConfirm.Text))
        //    {
        //        EntryConfirm.TextColor = Color.Red;
        //    }
        //}

        //private void EntryConfirm_Focused(object sender, FocusEventArgs e)
        //{
        //    EntryConfirm.TextColor = Color.Black;
        //}

        //public bool IsEmail(string Email)
        //{
        //    Email = Email == null ? "" : Email;
        //    string RegexMatcher = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
        //    Regex Re = new Regex(RegexMatcher, RegexOptions.IgnoreCase);

        //    if (Re.IsMatch(Email))
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        //public bool IsPhone(String Phone)
        //{
        //    Phone = Phone == null ? "" : Phone;
        //    string RegexMatcher = "\\(?\\d{3}\\)?-? *\\d{3}-? *-?\\d{4}";
        //    Regex Re = new Regex(RegexMatcher, RegexOptions.IgnoreCase);

        //    if (Re.IsMatch(Phone))
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        //public bool ValidateEmailUnused(String Email)
        //{
        //    //TODO tembak backend dapetin list email terus verify
        //    return true;
        //}

        //public bool ValidatePhoneUnused(String Phone)
        //{
        //    //TODO tembak backend dapetin list phone number terus verify
        //    return true;
        //}

        //public bool ValidatePassword(string Password, String ConfirmPassword)
        //{
        //    Password = Password == null ? "" : Password;
        //    ConfirmPassword = ConfirmPassword == null ? "" : ConfirmPassword;
        //    return Password == ConfirmPassword;
        //}
    }
}