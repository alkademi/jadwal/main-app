﻿using System;
using Xamarin.Forms.Xaml;
using jadwal_mobile.ViewModels;

namespace jadwal_mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DashboardView : Xamarin.Forms.TabbedPage
    {
        private HariIniView viewToday;
        private AcaraView acaraView;
        private ProfileView profileView;

        public DashboardView()
        {
            InitializeComponent();
            
            viewToday = this.Children[0] as HariIniView;
            acaraView = this.Children[3] as AcaraView;
            profileView = this.Children[4] as ProfileView;
        }

        private void ProfileView_Appearing(object sender, EventArgs e)
        {
            profileView.ProfileViewModel.FetchUser();
        }

        private void AcaraView_Appearing(object sender, EventArgs e)
        {
            acaraView.ListEventViewModel.FetchEvent();
        }

        private void HariIniView_Appearing(object sender, EventArgs e)
        {
            viewToday.TodayEventViewModel.FetchToday();
        }
    }
}