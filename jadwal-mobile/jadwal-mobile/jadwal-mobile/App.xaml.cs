using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

using jadwal_mobile.Views;

namespace jadwal_mobile
{
    public partial class App : Xamarin.Forms.Application
    {

        public App()
        {
            InitializeComponent();

            //Container.Register();

            var loginView = new LoginView();
            //var dashboard = new DashboardView();
            MainPage = new NavigationPage(loginView);
            NavigationPage.SetHasNavigationBar(loginView, false); //comment this if main page is dashboard
        
            App.Current.On<Xamarin.Forms.PlatformConfiguration.Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
